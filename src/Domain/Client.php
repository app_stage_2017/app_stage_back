<?php 

namespace apiv1\Domain;


class Client {
	
	/**
	 * Client id.
	 *
	 * @var integer
	 */
	private $idClient;
	
	/**
	 * Client Name.
	 *
	 * @var string
	 */
	private $nameClient;
	
	/**
	 * Client Code.
	 *
	 * @var string
	 */
	private $codeClient;
	
	
	
	public function getId() {
		return $this->idClient;
	}
	
	public function setId($idClient) {
		$this->idClient = $idClient;
		return $this;
	}
	
	public function getNameClient() {
		return $this->nameClient;
	}
	
	public function setNameClient($nameClient) {
		$this->nameClient = $nameClient;
		return $this;
	}
	
	public function getCodeClient() {
		return $this->codeClient;
	}
	
	public function setCodeClient($codeClient) {
		$this->codeClient = $codeClient;
		return $this;
	}
	
	
	
	
	
	
}