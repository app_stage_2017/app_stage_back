<?php

namespace apiv1\Domain;

use Symfony\Component\Security\Core\User\UserInterface;


class User implements UserInterface
{
	/**
	 * User id.
	 *
	 * @var integer
	 */
	private $id;
	
	
	

	/**
	 * User name.
	 *
	 * @var string
	 */
	private $username;
	
	/**
	 * User Lastname.
	 *
	 * @var string
	 */
	private $userLastname;
	
	/**
	 * User trigramme.
	 *
	 * @var string
	 */
	private $userTrigramme;

	/**
	 * User Login.
	 *
	 * @var string
	 */
	private $userLogin;
	
	/**
	 * User password.
	 *
	 * @var string
	 */
	private $password;
	
	/**
	 * User creationDate.
	 *
	 * @var \DateTime
	 */
	private $userCreationDate;
	
	
	
	/**
	 * User usertype.
	 *
	 * @var string
	 */
	private $userType;
	

	/**
	 * Salt that was originally used to encode the password.
	 *
	 * @var string
	 */
	
	
// specifique a userinteface en commentaire pour l'instant obliger de les d�clarer
	private $salt;

	/**
	 * Role.
	 * Values : ROLE_USER or ROLE_ADMIN.
	 *
	 * @var string
	 */
	
	private $role;
	
	

	public function getId() {
		return $this->id;
	}

	public function setId($id) {
		$this->id = $id;
		return $this;
	}

	/**
	 * @inheritDoc
	 */
	public function getUsername() {
		return $this->username;
	}

	public function setUsername($username) {
		$this->username = $username;
		return $this;
	}

	/**
	 * @inheritDoc
	 */
	public function getUserLastname() {
		return $this->userLastname;
	}
	
	public function setUserLastname($userLastname) {
		$this->userLastname = $userLastname;
		return $this;
	}
	
	/**
	 * @inheritDoc
	 */
	public function getUserTrigramme() {
		return $this->userTrigramme;
	}
	
	public function setUserTrigramme($userTrigramme) {
		$this->userTrigramme = $userTrigramme;
		return $this;
	}
	
	/**
	 * @inheritDoc
	 */
	public function getUserLogin() {
		return $this->userLogin;
	}
	
	public function setUserLogin($userLogin) {
		$this->userLogin = $userLogin;
		return $this;
	}
	
	/**
	 * @inheritDoc
	 */
	public function getPassword() {
		return $this->password;
	}

	public function setPassword($password) {
		$this->password = $password;
		return $this;
	}
	
	/**
	 * @inheritDoc
	 */
	public function getUserCreationDate() {
		return $this->userCreationDate;
	}
	
	public function setUserCreationDate($userCreationDate) {
		$this->userCreationDate = $userCreationDate;
		return $this;
	}
	
	
	
	/**
	 * @inheritDoc
	 */
	public function getUserType() {
		return $this->userType;
	}
	
	public function setUserType($userType) {
		$this->userType = $userType;
		return $this;
	}

	
	
// SPECIFIQUE A USER INTERFACE EN COMMENT POUR L'INSTANT	
	/**
	 * @inheritDoc
	 */
	
	public function getSalt()
	{
		return $this->salt;
	}

	public function setSalt($salt)
	{
		$this->salt = $salt;
		return $this;
	}

	public function getRole()
	{
		return $this->role;
	}

	public function setRole($role) {
		$this->role = $role;
		return $this;
	}

	/**
	 * @inheritDoc
	 */
	
	public function getRoles()
	{
		return array($this->getRole());
	}

	/**
	 * @inheritDoc
	 */
	
	public function eraseCredentials() {
		// Nothing to do here
	}
	
	
	
}





