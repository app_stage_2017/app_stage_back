<?php 

namespace apiv1\Domain;

class LogTaskHistory {
	
	/**
	 * LogTaskHistory id.
	 *
	 * @var integer
	 */
	private $id;
	
	
	
	/**
	 * Associated idTaskProject.
	 *
	 * @var integer
	 */
	private $fktask;
	
	/**
	 * LogTaskHistory Date.
	 *
	 * @var \DateTime
	 */
	private $date;
	
	/**
	 * LogTaskHistory userTrigramme.
	 *
	 * @var string
	 */
	private $userTrigramme;
	
	/**
	 * LogTaskHistory typeLog.
	 *
	 * @var string
	 */
	private $typeLog;
	
	/**
	 * LogTaskHistory commentary.
	 *
	 * @var string
	 */
	private $commentary;
	
	public function getId() {
		return $this->id;
	}
	
	public function setId($id) {
		$this->id = $id;
		return $this;
	}
	
	
	
	public function getfkTask() {
		return $this->fktask;
	}
	
	public function setfkTask($fkTask) {
		$this->fktask = $fkTask;
		return $this;
	}
	
	public function getDate() {
		return $this->date;
	}
	
	public function setDate($date) {
		$this->date = $date;
		return $this;
	}
	
	public function getUserTrigramme() {
		return $this->userTrigramme;
	}
	
	public function setUserTrigramme($userTrigramme) {
		$this->userTrigramme = $userTrigramme;
		return $this;
	}
	
	public function getTypeLog() {
		return $this->typeLog;
	}
	
	public function setTypeLog($typeLog) {
		$this->typeLog = $typeLog;
		return $this;
	}
	
	public function getCommentary() {
		return $this->commentary;
	}
	
	public function setCommentary($commentary) {
		$this->commentary = $commentary;
		return $this;
	}
	
	
	
}
