<?php

namespace apiv1\Domain;

class TaskProject
{
    /**
     * TaskProject id.
     *
     * @var integer
     */
    private $id;

    /**
     * TaskProject name.
     *
     * @var string
     */
    private $name;

    /**
     * TaskProject description.
     *
     * @var string
     */
    private $description;
    
    /**
     * TaskProject statut.
     *
     * @var string
     */
    private $statut;
    
    /**
     * TaskProject commentary.
     *
     * @var string
     */
    private $commentary;
    
    /**
     * TaskProject category.
     *
     * @var string
     */
    private $category;
    
    /**
     * TaskProject creationDate.
     *
     * @var \DateTime
     */
    private $creationDate;
    
    /**
     * TaskProject endDate.
     *
     * @var \DateTime
     */
    private $endDate;
    
   
    /**
     * Associated project.
     *
     * @var \apiv1\Domain\Project
     */
    private $project;
    
    /**
     * TaskProject userType.
     *
     * @var string
     */
    private $userType;

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    public function getDescription() {
        return $this->description;
    }

    public function setDescription($description) {
        $this->description = $description;
        return $this;
    }
    
    public function getStatut() {
    	return $this->statut;
    }
    
    public function setStatut($statut) {
    	$this->statut = $statut;
    	return $this;
    }
    
    public function getCommentary() {
    	return $this->commentary;
    }
    
    public function setCommentary($commentary) {
    	$this->commentary = $commentary;
    	return $this;
    }
    
    public function getCategory() {
    	return $this->category;
    }
    
    public function setCategory($category) {
    	$this->category = $category;
    	return $this;
    }
    
    public function getCreationDate() {
    	return $this->creationDate;
    }
    
    public function setCreationDate($creationDate) {
    	$this->creationDate = $creationDate;
    	return $this;
    }
    
    public function getEndDate() {
    	return $this->endDate;
    }
    
    public function setEndDate($endDate) {
    	$this->endDate = $endDate;
    	return $this;
    }

    public function getProject() {
        return $this->project;
    }

    public function setProject(Project $project) {
        $this->project = $project;
        return $this;
    }
    
    public function getUserType() {
    	return $this->userType;
    }
    
    public function setUserType($userType) {
    	$this->userType = $userType;
    	return $this;
    }
    
}