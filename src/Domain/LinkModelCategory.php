<?php 

namespace apiv1\Domain;


class LinkModelCategory {
	
	/**
	 * LinkModelCategory id.
	 *
	 * @var integer
	 */
	private $idLinkModelCategory;
	
	
	/**
	 * Model id.
	 *
	 * @var integer
	 */
	private $idModel;
	
	/**
	 * Category id.
	 *
	 * @var integer
	 */
	private $idCategory;
	
	public function getIdLinkModelCategory() {
		return $this->idLinkModelCategory;
	}
	
	public function setLinkModelCategory($idLinkModelCategory) {
		$this->idLinkModelCategory = $idLinkModelCategory;
		return $this;
	}
	
	public function getIdModel() {
		return $this->idModel;
	}
	
	public function setIdModel($idModel) {
		$this->idModel = $idModel;
		return $this;
	}
	
	public function getidCategory() {
		return $this->idCategory;
	}
	
	public function setidCategory($idCategory) {
		$this->idCategory = $idCategory;
		return $this;
	}
		
}