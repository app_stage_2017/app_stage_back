<?php

namespace apiv1\Domain;

class Project 
{
    /**
     * Project id.
     *
     * @var integer
     */
    private $id;

    /**
     * Project Name.
     *
     * @var string
     */
    private $name;

    /**
     * Projet client.
     *
     * @var int
     */
    private $client;
    
    /**
     * Projet model.
     *
     * @var string
     */
    private $model;
    
    /**
     * Projet startdate.
     *
     * @var \DateTime
     */
    private $startdate;
    
    /**
     * Projet enddate.
     *
     * @var \DateTime
     */
    private $enddate;
    
    /**
     * Projet state.
     *
     * @var string
     */
    private $state;
    
    /**
     * Projet creator.
     *
     * @var string
     */
    private $creator;
    
    /**
     * Associated user.
     *
     * @var \apiv1\Domain\User
     */
    private $user;

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    public function getClient() {
    	return $this->client;
    }
    
    public function setClient($client) {
    	$this->client = $client;
    	return $this;
    }
    
    public function getModel() {
    	return $this->model;
    }
    
    public function setModel($model) {
    	$this->model = $model;
    	return $this;
    }
    
    public function getStartDate() {
    	return $this->startdate;
    }
    
    public function setStartDate($startdate) {
    	$this->startdate = $startdate;
    	return $this;
    }
    
    public function getEndDate() {
    	return $this->enddate;
    }
    
    public function setEndDate($enddate) {
    	$this->enddate = $enddate;
    	return $this;
    }
    
    public function getState() {
    	return $this->state;
    }
    
    public function setState($state) {
    	$this->state = $state;
    	return $this;
    }
    
    public function getCreator() {
    	return $this->creator;
    }
    
    public function setCreator($creator) {
    	$this->creator = $creator;
    	return $this;
    }
    
    public function getUser() {
    	return $this->user;
    }
    
    public function setUser(User $user) {
    	$this->user = $user;
    	return $this;
    }
}
