<?php 

namespace apiv1\Domain;


class AssignProjectUser {
	
	/**
	 * AssignProjectUser id.
	 *
	 * @var integer
	 */
	private $idAssignProjectUser;
	
	
	/**
	 * Project fk.
	 *
	 * @var integer
	 */
	private $fkProject;
	
	/**
	 * User fk.
	 *
	 * @var integer
	 */
	private $fkUser;
	
	public function getIdAssignProjectUser() {
		return $this->idAssignProjectUser;
	}
	
	public function setIdAssignProjectUser($idAssignProjectUser) {
		$this->idAssignProjectUser = $idAssignProjectUser;
		return $this;
	}
	
	public function getFkProject() {
		return $this->fkProject;
	}
	
	public function setFkProject($fkProject) {
		$this->fkProject = $fkProject;
		return $this;
	}
	
	public function getFkUser() {
		return $this->fkUser;
	}
	
	public function setFkUser($fkUser) {
		$this->fkUser = $fkUser;
		return $this;
	}
		
}