<?php


namespace apiv1\Domain;


class AttachedFile 
{
	
	/**
	 * AttachedFile id.
	 *
	 * @var integer
	 */
	private $id;
	
	/**
	 * AttachedFile Name.
	 *
	 * @var string
	 */
	private $name;
	
	/**
	 * AttachedFile path.
	 *
	 * @var string
	 */
	private $path;
	
	/**
	 * AttachedFile idTask.
	 *
	 * @var integer
	 */
	
	private $idTask;
	
	public function getId() {
		return $this->id;
	}
	
	public function setId($id) {
		$this->id = $id;
		return $this;
	}
	
	public function getName() {
		return $this->name;
	}
	
	public function setName($name) {
		$this->name = $name;
		return $this;
	}
	
	public function getPath() {
		return $this->path;
	}
	
	public function setPath($path) {
		$this->path = $path;
		return $this;
	}
	
	public function getIdTask() {
		return $this->idTask;
	}
	
	public function setIdTask($idTask) {
		$this->idTask = $idTask;
		return $this;
	}
	
	
	
	
}