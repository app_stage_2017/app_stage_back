<?php 

namespace apiv1\Domain;

class Task {
	
	/**
	 * Task id.
	 *
	 * @var integer
	 */
	private $id;
	
	/**
	 * Task Name.
	 *
	 * @var string
	 */
	private $name;
	
	/**
	 * Associated category.
	 *
	 * @var string
	 */
	private $category;
	
	/**
	 * Task Description.
	 *
	 * @var string
	 */
	private $description;
	
	/**
	 * Task categoryTask.
	 *
	 * @var string
	 */
	private $categoryTask;
	
	/**
	 * Task userType.
	 *
	 * @var string
	 */
	private $userType;
	
	public function getId() {
		return $this->id;
	}
	
	public function setId($id) {
		$this->id = $id;
		return $this;
	}
	
	public function getName() {
		return $this->name;
	}
	
	public function setName($name) {
		$this->name = $name;
		return $this;
	}
	
	public function getDescription() {
		return $this->description;
	}
	
	public function setDescription($description) {
		$this->description = $description;
		return $this;
	}
	
	public function getCategory() {
		return $this->category;
	}
	
	public function setCategory( $category) {
		$this->category = $category;
		return $this;
	}
	
	public function getCategoryTask() {
		return $this->categoryTask;
	}
	
	public function setCategoryTask($categoryTask) {
		$this->categoryTask = $categoryTask;
		return $this;
	}
	
	public function getUserType() {
		return $this->userType;
	}
	
	public function setUserType($userType) {
		$this->userType = $userType;
		return $this;
	}

}
	
	
	
	
	
	
	
	
