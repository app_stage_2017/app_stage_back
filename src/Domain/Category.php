<?php 

namespace apiv1\Domain;


class Category {
	
	/**
	 * Model id.
	 *
	 * @var integer
	 */
	private $id;
	
	/**
	 * Model Name.
	 *
	 * @var string
	 */
	private $name;
	
	/**
	 * Associated model.
	 *
	 * @var \apiv1\Domain\Model
	 */
	private $model;
	
	public function getId() {
		return $this->id;
	}
	
	public function setId($id) {
		$this->id = $id;
		return $this;
	}
	
	public function getName() {
		return $this->name;
	}
	
	public function setName($name) {
		$this->name = $name;
		return $this;
	}
	
	public function getModel() {
		return $this->model;
	}
	
	public function setModel(Model $model) {
		$this->model = $model;
		return $this;
	}
		
}