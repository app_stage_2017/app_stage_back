<?php

namespace apiv1\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\File;
use Symfony\Component\HttpFoundation\FileBag;
use apiv1\Domain\AttachedFile;


class  ApiFileController{
	
	/**
	 * API create File controller.
	 *
	 * @param Request $request Incoming request
	 * @param Application $app Silex application
	 *
	 * @return file details in JSON format
	 */
	public function createFile(Request $request, Application $app)
	
	{
		
		$dir = $app['upload.directory'];
	
		
		
		$file_bag = $request->files;
	
		
		if($file_bag->has('file'))
		{
			$filerecup=$file_bag->get('file');
			$filename = $request->request->get('filename');
			$idTask = $request->request->get('idTask');
			$projectname = $request->request->get('projectname');
		//	$folder = $dir.'\\'.$projectname; //pour win
			$folder = $dir.'/'.$projectname; //pour mac
			if(file_exists($folder))
			{
				$filerecup->move($folder,$filename);
			}
			else
			{
				mkdir($folder,0777,true);
				$filerecup->move($folder,$filename);
			}
			
			//$filerecup->move($dir,$filename);
		//	$path="http://localhost/apiv1/var/uploads/".$projectname.'/'.$filename; //  POUR WIn
		  $path = "http://localhost:8888/apiv1uploads/".$projectname.'/'.$filename; //POUR MAC
			
		
		 
		//	$responseData = $path;
			
					
		}
		
		// Build the new file
		$file = new AttachedFile();
			
		//Save the file Object
		$responseData = $this->saveFileObject($request, $file, $filename,$path, $idTask, $app);
			
		return $app->json($responseData,201);
		
			
	}
	
	/**
	 * save the task
	 *
	 *@param Request $request Incoming request
	 *@param File $file
	 *@param Application $app
	 *
	 * @return an array .
	 */
	private function saveFileObject(Request $request, AttachedFile $file,  $filename, $path,  $idTask, Application $app)
	{
	
		$file->setName($filename);
		$file->setPath($path);
		$file->setIdTask($idTask);
		
			
		$app['dao.attachedFile']->save($file);
		$responseData = $this->buildFileArray($file);
		return $responseData;
			
	}
	
	/**
	 * return a list of file into an associative array for JSON encoding
	 *
	 * @param array A list of all file for a task.
	 *
	 * @return array Associative array whose fields are the file properties.
	 */
	private function buildFileArray($file)
	
	{
				$data[]  = array(
						'id' => $file->getId(),
						'namefile'=>$file->getName(),
						'namepath'=>$file->getPath(),
						//'idTask'=>$file->getIdTask()
							
				);
			
		
		return $data;
	}
	
	
	
	
	
	
}