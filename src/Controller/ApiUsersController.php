<?php

namespace apiv1\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use apiv1\Domain\User;


class ApiUsersController {
	/**
	 * API user controller.
	 *
	 * @param Application $app Silex application
	 *
	 * @return All users in JSON format
	 */
	
	public function getUsers(Application $app)
	
	{
		$users = $app['dao.user']->findAll();
		//var_dump($users);
		// Convert an array of objects ($users) into an array of associative users ($responseData)
		$responseData = array();
		foreach ($users as $user) {
			$responseData[] = $this->buildUserArray($user,$app);
		}
		// Create and return a JSON response
		return $app->json($responseData);
	}
	
	/**
	 * API create user controller.
	 *
	 * @param Request $request Incoming request
	 * @param Application $app Silex application
	 *
	 * @return User details in JSON format
	 */
	public function createUser(Request $request, Application $app)
	
	{
		// Check request parameters
		$this->checkRequestParameters($request,$app);
		 
		 
		// Build the new user
		$user = new User();
		 
		//Save the user Object
		$responseData = $this->saveUserObject($request, $user, $app);
		 
		return $app->json($responseData, 201);
	}
	
	
	/**
	 * API delete user controller.
	 *
	 * @param integer $id User id
	 * @param Application $app Silex application
	 */
	public function deleteUser($id, Application $app)
	
	{
		// Delete the user
		$app['dao.user']->delete($id);
		return $app->json('No Content', 204);  // 204 = No content
	}
	
	
	/**
	 * Converts an User object into an associative user for JSON encoding
	 *
	 * @param User $user User object
	 *
	 * @return array Associative user whose fields are the user properties.
	 */
	
	
	private function buildUserArray(User $user, $app)
	
	{
		$data  = array(
				'id' => $user->getId(),
				'userName' => $user->getUsername(),
				'lastName' => $user->getUserLastname(),
				'userTrigramme' => $user->getUserTrigramme(),
			//	'login' => $user->getUserLogin(),
			//	'password' => $user->getPassword(),
				'creationDate' => $user->getUserCreationDate(),
				'userType' => $user->getUserType(),
			//	'tasks' => $this->buildTaskArray($app['dao.taskproject']->findAllByproject($project->getId()))
		);
		return $data;
	}
	
	/**
	 * check the request parameters
	 *
	 *@param Request $request Incoming request
	 *@param Application $app Silex application
	 *
	 * @return an json with code 400 if the parameters are false.
	 */
	
	private function checkRequestParameters(Request $request, Application $app)
	{
		// Check request parameters
		if (!$request->request->has('userName')) {
			return $app->json('Missing required parameter: userName', 400);
		}
		if (!$request->request->has('lastName')) {
			return $app->json('Missing required parameter: lastName', 400);
		}
		if (!$request->request->has('userTrigramme')) {
			return $app->json('Missing required parameter: userTrigramme', 400);
		}
		if (!$request->request->has('login')) {
			return $app->json('Missing required parameter: login', 400);
		}
		if (!$request->request->has('password')) {
			return $app->json('Missing required parameter: password', 400);
		}
		if (!$request->request->has('creationDate')) {
			return $app->json('Missing required parameter: creationDate', 400);
		}
		if (!$request->request->has('userType')) {
			return $app->json('Missing required parameter: userType', 400);
		}
	}
	
	/**
	 * save the user
	 *
	 *@param Request $request Incoming request
	 *@param User $user
	 *@param Application $app
	 *
	 * @return an array .
	 */
	private function saveUserObject(Request $request, User $user, Application $app)
	{
		$user->setUsername($request->request->get('userName'));
		$user->setUserLastname($request->request->get('lastName'));
		$user->setUserTrigramme($request->request->get('userTrigramme'));
		$user->setUserLogin($request->request->get('login'));
		$user->setPassword($request->request->get('password'));
		$user->setUserCreationDate($request->request->get('creationDate'));
		$user->setUserType($request->request->get('userType'));
		 
		$app['dao.user']->save($user);
		$responseData = $this->buildUserArray($user,$app);
		return $responseData;
		 
	}
	
	
}