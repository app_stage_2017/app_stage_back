<?php

namespace apiv1\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use apiv1\Domain\Model;
use apiv1\Domain\Category;
use apiv1\Domain\LinkModelCategory;
use apiv1\Domain\Task;



class ApiModelsCategoryTaskController {

//----------------------------------------------------------------------------------------------------------------//
//                                                 Models Controller                                              //
//----------------------------------------------------------------------------------------------------------------//
	
	
	
    /**
     * API model controller.
     *
     * @param Application $app Silex application
     *
     * @return All model in JSON format
     */
    
    public function getModels(Application $app)
    
    {
        $models = $app['dao.model']->findAll();
        // Convert an array of objects ($models) into an array of associative models ($responseData)
        $responseData = array();
        foreach ($models as $model) {
            $responseData[] = $this->buildModelArray($model,$app);
        }
        // Create and return a JSON response
        return $app->json($responseData);
    }
    
    /**
     * API delete model controller.
     *
     * @param integer $id Model id
     * @param Application $app Silex application
     */
    public function deleteModel($id, Application $app)
    
    {
    	// Delete the project
    	$app['dao.model']->delete($id);
    	return $app->json('No Content', 204);  // 204 = No content
    }
    
    /**
     * API create model controller.
     *
     * @param Request $request Incoming request
     * @param Application $app Silex application
     *
     * @return Model details in JSON format
     */
    public function createModel(Request $request, Application $app)
    
    {
    	// Check request parameters
    	if (!$request->request->has('name')) {
    		return $app->json('Missing required parameter: name', 400);
    	}
    	  
    	// Build the new project
    	$model = new Model();
    	 
    	//Save the project Object
    	$model->setName($request->request->get('name'));
    	$app['dao.model']->save($model);
    	$responseData = $this->buildModelArray($model,$app);
    	 
    	return $app->json($responseData, 201);
    }
    
    /**
     * Converts an Model object into an associative category for JSON encoding
     *
     * @param Model $model Model object
     *
     * @return array Associative model whose fields are the pcategory properties.
     */
    
    
    private function buildModelArray(Model $model, $app)
    
    {
    	$data  = array(
    			'id' => $model->getId(),
    			'name' => $model->getName(),
    			//	'category' => $this->buildCategoryArray($app['dao.category']->findCategoryByModel($model->getId()),$app)
    			 
    	);
    	return $data;
    }
    
 //----------------------------------------------------------------------------------------------------------------//
 //                                                 LinkModelCategory Controller                                              //
 //----------------------------------------------------------------------------------------------------------------//
    /**
     * API create LinkModelCategory controller.
     *
     * @param Request $request Incoming request
     * @param Application $app Silex application
     *
     * @return LinkModelCategory details in JSON format
     */
    public function linkModelCategory(Request $request, Application $app)
    
    {
    	// Check request parameters
    	if (!$request->request->has('fkModel')) {
    		return $app->json('Missing required parameter: fkModel', 400);
    	}
    	if (!$request->request->has('fkCategory')) {
    		return $app->json('Missing required parameter: fkCategory', 400);
    	}
    		
    	// Build the new linkModelCategory
    	$linkModelCategory = new LinkModelCategory();
    		
    	//Save the linkModelCategory Object
    	$responseData = $this->savelinkModelCategoryObject($request, $linkModelCategory, $app);
    		
    	return $app->json($responseData, 201);
    }
    
    /**
     * save the linkModelCategory
     *
     *@param Request $request Incoming request
     *@param $linkModelCategory
     *@param Application $app
     *
     * @return an array .
     */
    private function savelinkModelCategoryObject(Request $request, LinkModelCategory $linkModelCategory, Application $app)
    {
    	$linkModelCategory->setIdModel($request->request->get('fkModel'));
    	$linkModelCategory->setidCategory($request->request->get('fkCategory'));
    
    		
    	$app['dao.linkmodelcategory']->save($linkModelCategory);
    	$responseData = $this->buildlinkModelCategory($linkModelCategory,$app);
    	return $responseData;
    		
    }
    
    private function buildlinkModelCategory(LinkModelCategory $linkModelCategory, $app)
    
    {
    	$data  = array(
    			'id' => $linkModelCategory->getIdLinkModelCategory(),
    			'fkModel' => $linkModelCategory->getIdModel(),
    			'fkCategory' => $linkModelCategory->getidCategory()
    	);
    	return $data;
    }
    
    
//----------------------------------------------------------------------------------------------------------------//
//                                                 Categories Controller                                              //
//----------------------------------------------------------------------------------------------------------------//
    
    /**
     *
     * @param Application $app Silex application
     *
     * @return All catgeory in JSON format
     */
  
    public function getCategories(Application $app)
    
    {
    	$categories = $app['dao.category']->findAll();
    	// Convert an array of objects ($categories) into an array of associative projects ($responseData)
    	$responseData = array();
    	foreach ($categories as $category) {
    		$responseData[] = $this->buildAllCategoryArray($category,$app);
    	}
    	// Create and return a JSON response
    	return $app->json($responseData);
    }
    
    /**
     * API category details controller.
     *
     * @param integer $id modelid id
     * @param Application $app Silex application
     *
     * @return category details in JSON format
     */
    
     public function getCategoryByIdModel($id, Application $app) {
     $categories = $app['dao.category']->findCategoryByModel($id);
     $responseData = $this->buildCategoryArray($categories, $app);
     // Create and return a JSON response
     return $app->json($responseData);
     }
     
     /**
      * API delete model controller.
      *
      * @param integer $id Model id
      * @param Application $app Silex application
      */
     public function deleteCategory($id, Application $app)
     
     {
     	// Delete all associated tasks
     	//  $app['dao.taskproject']->deleteAllByArticle($id);
     	// Delete the project
     	$app['dao.category']->delete($id);
     	return $app->json('No Content', 204);  // 204 = No content
     }
     
     /**
      * API create category controller.
      *
      * @param Request $request Incoming request
      * @param Application $app Silex application
      *
      * @return Category details in JSON format
      */
     public function createCategory(Request $request, Application $app)
     
     {
     	// Check request parameters
     	if (!$request->request->has('name')) {
     		return $app->json('Missing required parameter: name', 400);
     	}
     	 
     	// Build the new category
     	$category = new Category();
     
     	//Save the category Object
     	$category->setName($request->request->get('name'));
     	$app['dao.category']->save($category);
     	$responseData = $this->buildAllCategoryArray($category, $app);
     
     	return $app->json($responseData, 201);
     }
     
     
     /**
      * Converts an Category object into an associative category for JSON encoding
      *
      * @param Category $category Category object
      *
      * @return array Associative category whose fields are the pcategory properties.
      */
     
     /*   en commentaires, car a utiliser si on fait une m�thode get model complete (cad avec categories et task int�gr�es, mais requ�tes longues
      */
     
     
     private function buildCategoryArray($categories, $app)
     
     {
     	if(empty($categories))
     	{
     		$data=[];
     	}
     	else
     	{
     		foreach ($categories as $category)
     		{
     			$data[]  = array(
     					'id' => $category->getId(),
     					'name' => $category->getName(),
     					'task' => $this->buildTaskArray($app['dao.task']->findTaskByCategory($category->getId()),$app)
     			);
     		}
     		//return $data;
     	}
     	return $data;
     }
     
  
      private function buildAllCategoryArray(Category $category, $app)
     
      {
      $data  = array(
      'id' => $category->getId(),
      'name' => $category->getName(),
      'task' => $this->buildTaskArray($app['dao.task']->findTaskByCategory($category->getId()),$app)
       
     
      );
      return $data;
      }

    
 //----------------------------------------------------------------------------------------------------------------//
 //                                                 Tasks Controller                                             //
 //----------------------------------------------------------------------------------------------------------------//
    
 // A FAIRE
 
      /**
       * API create taskModel controller.
       *
       * @param Request $request Incoming request
       * @param Application $app Silex application
       *
       * @return Task details in JSON format
       */
      
      public function createTaskModel(Request $request, Application $app)
       
      {
      	// Check request parameters
      	if (!$request->request->has('name')) {
      		return $app->json('Missing required parameter: name', 400);
      	}
      	if (!$request->request->has('description')) {
      		return $app->json('Missing required parameter: description', 400);
      	}
      	if (!$request->request->has('categoryName')) {
      		return $app->json('Missing required parameter: categorie', 400);
      	}
      	if (!$request->request->has('categoryId')) {
      		return $app->json('Missing required parameter: categorie', 400);
      	}
      	if (!$request->request->has('userType')) {
      		return $app->json('Missing required parameter: usertype', 400);
      	}
      	 
      	// Build the new category
      	$task = new Task();
      	 
      	//Save the task Object
      	$task->setName($request->request->get('name'));
      	$task->setDescription($request->request->get('description'));
      	$task->setCategoryTask($request->request->get('categoryName'));
      	$task->setUserType($request->request->get('userType'));
      	$task->setCategory($request->request->get('categoryId'));
      	$app['dao.task']->save($task);
      	$responseData = $this->buildTaskArray2($task, $app);
      	 
      	return $app->json($responseData, 201);
      }
 
    
    /**
     * Converts an Task object into an associative task for JSON encoding
     *
     * @param Task $task Task object
     *
     * @return array Associative task whose fields are the task properties.
     */
    
    
    private function buildTaskArray($tasks,$app)
    
    {
    	if(empty($tasks)){
    		$data=[];
    	}
    	else {
    		foreach ($tasks as $task) {
    			$data[]  = array(
    					'id' => $task->getId(),
    					'name' => $task->getName(),
    					'description' => $task->getDescription(),
    				//	'fkproject'=>$task->getCategory()->getId(),
    					'categorie' => $task->getCategoryTask(),
    					'usertype' => $task->getUsertype(),
    				//	'modelAttachedFile'=> $this->buildModelAttachedFileArray($app['dao.modelattachedfile']->findFileByTask($task->getId()))
    			);
    		}
    		return $data;
    	}
    }
    
    private function buildTaskArray2(Task $task, $app)
     
    {
    	$data  = array(
    			'id' => $task->getId(),
    			'taskName' => $task->getName(),
    			'taskDescription' => $task->getDescription(),
    			'categoryTask'=> $task->getCategoryTask(),
    			'userType'=> $task->getUserType(),
    			'fk-category' => $task->getCategory(),
    			 
    			 
    	);
    	return $data;
    }
    
    /**
     * return a list of file into an associative array for JSON encoding
     *
     * @param array A list of all file for a task.
     *
     * @return array Associative array whose fields are the log properties.
     */
    private function buildModelAttachedFileArray($files)
    
    {
    	if(empty($files)){
    		$data=[];
    	}
    	else {
    		foreach ($files as $file) {
    			$data[]  = array(
    					'id' => $file->getId(),
    					'namefile'=>$file->getName(),
    					'namepath'=>$file->getPath(),
    					//		'idTask'=>$file->getIdTask()
    						
    			);
    		}
    	}
    	return $data;
    }
    
    
}    