<?php 

namespace apiv1\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use apiv1\Domain\TaskProject;
use apiv1\Domain\Project;
use apiv1\DAO\ProjectDAO;

class ApiTasksProjectsController {
	
	
	/**
	 * API task controller.
	 *
	 * @param Application $app Silex application
	 *
	 * @return All taks in JSON format
	 */
	
/*	
	public function getTasks(Application $app)
	
	{
		$tasks = $app['dao.taskproject']->findAll();
		// Convert an array of objects ($projects) into an array of associative projects ($responseData)
		$responseData = array();
		foreach ($tasks as $task) {
			$responseData[] = $this->buildTaskArray($task,$app);
		}
		// Create and return a JSON response
		return $app->json($responseData);
	}
*/	
	/**
	 * API taskProject details controller.
	 *
	 * @param integer $id TaskProject id
	 * @param Application $app Silex application
	 *
	 * @return TaskProject details in JSON format
	 */
/*	 
	public function getTaskById($id, Application $app) {
		$task = $app['dao.taskproject']->find($id);
		$responseData = $this->buildTaskArray($task,$app);
		// Create and return a JSON response
		return $app->json($responseData);
	}
*/	
	/**
	 * API create TaskProject controller.
	 *
	 * @param Request $request Incoming request
	 * @param Application $app Silex application
	 *
	 * @return taskProject details in JSON format
	 */
	public function createTaskProject(Request $request, Application $app)
	
	{
		// Check request parameters
		$this->checkRequestParametersUpdate($request,$app);
		 
		 
		// Build the new taskproject
		$taskProject = new TaskProject();
		 
		//Save the project Object
		$responseData = $this->saveTaskProjectObjectCreate($request, $taskProject, $app);
		 
		return $app->json($responseData, 201);
	}
	
	
	
	
	/**
	 * API update task controller with put method.
	 *
	 * @param integer $id TaskProject id
	 * @param Request $request Incoming request
	 * @param Application $app Silex application
	
	 *
	 * @return Project details in JSON format
	 */
	
	public function updateTaskById($id,Request $request, Application $app)
	 
	{	// Check request parameters
	$this->checkRequestParametersUpdate($request,$app);
	 
	//find the task by id
	$task=$task = $app['dao.taskproject']->find($id);
	 
	//Save the task Object
	$responseData = $this->saveTaskProjectObjectUpdate($request, $task, $app);
	
	return $app->json($responseData, 200);
	
	}
	
	/**
	 * API delete task controller.
	 *
	 * @param integer $id Task id
	 * @param Application $app Silex application
	 */
	public function deleteTask($id, Application $app)
	
	{
		$app['dao.taskproject']->delete($id);
		return $app->json('No Content', 204);  // 204 = No content
	}
	
	/**
	 * Converts an Task object into an associative task for JSON encoding
	 *
	 * @param TaskProject $task TaskProject object
	 *
	 * @return array Associative project whose fields are the task properties.
	 */
	
	
	private function buildTaskArray(TaskProject $task, $app)
	
	{
		$data  = array(
				'id' => $task->getId(),
    			'name' => $task->getName(),
    			'description'=>$task->getDescription(),
    			'statut'=>$task->getStatut(),
				'commentary'=>$task->getCommentary(),
    			'category'=>$task->getCategory(),
    			'creationDate'=>$task->getCreationDate(),
    			'endDate'=>$task->getEndDate(),
				'fkproject'=>$task->getProject()->getId(),
    			'userType'=>$task->getUserType()
		);
		return $data;
	}
	
	/**
	 * check the request parameters
	 *
	 *@param Request $request Incoming request
	 *@param Application $app Silex application
	 *
	 * @return an json with code 400 if the parameters are false.
	 */
	
	private function checkRequestParametersUpdate(Request $request, Application $app)
	{
		// Check request parameters
		if (!$request->request->has('name')) {
			return $app->json('Missing required parameter: name', 400);
		}
		if (!$request->request->has('description')) {
			return $app->json('Missing required parameter: description', 400);
		}
		if (!$request->request->has('statut')) {
			return $app->json('Missing required parameter: statut', 400);	
		}
		if (!$request->request->has('commentary')) {
			return $app->json('Missing required parameter: commentary', 400);
		}	
		if (!$request->request->has('category')) {
			return $app->json('Missing required parameter: category', 400);
		}
		if (!$request->request->has('creationDate')) {
			return $app->json('Missing required parameter: creationDate', 400);
		}
		if (!$request->request->has('endDate')) {
			return $app->json('Missing required parameter: endDate', 400);
		}
	//	if (!$request->request->has('fkproject')) {
	//		return $app->json('Missing required parameter: fkproject', 400);
	//	}
		if (!$request->request->has('userType')) {
			return $app->json('Missing required parameter: userType', 400);
		}
	}
	
	/**
	 * save the task
	 *
	 *@param Request $request Incoming request
	 *@param Task $task
	 *@param Application $app
	 *
	 * @return an array .
	 */
	private function saveTaskProjectObjectUpdate(Request $request, TaskProject $task, Application $app)
	{
		
		$task->setName($request->request->get('name'));
		$task->setDescription($request->request->get('description'));
		$task->setStatut($request->request->get('statut'));
		$task->setCommentary($request->request->get('commentary'));
		$task->setCategory($request->request->get('category'));
		$task->setCreationDate($request->request->get('creationDate'));
		$task->setEndDate($request->request->get('endDate'));
	//	$task->setProject($app['dao.project']->find($request->request->get('fkproject')));
		$task->setUserType($request->request->get('userType'));
		 
		$app['dao.taskproject']->save($task);
		$responseData = $this->buildTaskArray($task,$app);
		return $responseData;
		 
	}
	
	/**
	 * save the task
	 *
	 *@param Request $request Incoming request
	 *@param Task $task
	 *@param Application $app
	 *
	 * @return an array .
	 */
	private function saveTaskProjectObjectCreate(Request $request, TaskProject $task, Application $app)
	{
	
		$task->setName($request->request->get('name'));
		$task->setDescription($request->request->get('description'));
		$task->setStatut($request->request->get('statut'));
		$task->setCommentary($request->request->get('commentary'));
		$task->setCategory($request->request->get('category'));
		$task->setCreationDate($request->request->get('creationDate'));
		$task->setEndDate($request->request->get('endDate'));
		$task->setProject($app['dao.project']->find($request->request->get('fkproject')));
		$task->setUserType($request->request->get('userType'));
			
		$app['dao.taskproject']->save($task);
		$responseData = $this->buildTaskArray($task,$app);
		return $responseData;
			
	}
	
	
	
	
}