<?php 

namespace apiv1\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use apiv1\Domain\User;


class ApiLoginController {
	
	/**
	 * API articles controller.
	 *
	 * @param Application $app Silex application, Request $request
	 *
	 * @return json response
	 */
	
	
	public function authenticate(Request $request,Application $app)
	
	{
	
		$userMessage = $request->request->get('login');
		$mdpMessage = $request->request->get('password');
		$user=$app['dao.user']->loadUserByUsername($userMessage);
		if($user){
			if($mdpMessage == $user->getPassword()){
			
				$reponsedata= [
						'id'=>$user->getId(),
						'name'=>$user->getUsername(),
						'lastname'=>$user->getUserLastname(),
						'trigramme'=>$user->getUserTrigramme(),
						'userType'=>$user->getUserType()
				
				];	
				$code = 200;
			}
			else{
				$reponsedata= [
						'utilisateur'=>"present et mais mauvais mdp"
				];
				$code = 403;
			}
				
		}
		else{
			$responsedata = [
					'utilisateur'=>"non present"
			];
			$code=404;
		}
		return $app->json($reponsedata,$code);
		
	}
		
}


