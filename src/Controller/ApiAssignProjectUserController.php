<?php

namespace apiv1\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use apiv1\Domain\AssignProjectUser;


class ApiAssignProjectUserController {
	
	/**
	 * API create assignprojectcontrolleur controller.
	 *
	 * @param Request $request Incoming request
	 * @param Application $app Silex application
	 *
	 * @return assignprojectcontrolleur details in JSON format
	 */
	public function assignProjetUser(Request $request, Application $app)
	
	{
		// Check request parameters
		if (!$request->request->has('fkUser')) {
			return $app->json('Missing required parameter: fkUser', 400);
		}
		if (!$request->request->has('fkProject')) {
			return $app->json('Missing required parameter: fkProject', 400);
		}
		 
		// Build the new assignProjectUser
		$assignProjectUser = new AssignProjectUser();
		 
		//Save the assignProjectUser Object
		$responseData = $this->saveAssignProjectUserObject($request, $assignProjectUser, $app);
		 
		return $app->json($responseData, 201);
	}
	
	/**
	 * save the assignProjectUser
	 *
	 *@param Request $request Incoming request
	 *@param $assignProjectUser 
	 *@param Application $app
	 *
	 * @return an array .
	 */
	private function saveAssignProjectUserObject(Request $request, AssignProjectUser $assignProjectUser, Application $app)
	{
		$assignProjectUser->setFkUser($request->request->get('fkUser'));
		$assignProjectUser->setFkProject($request->request->get('fkProject'));
		
		 
		$app['dao.assignprojectuser']->save($assignProjectUser);
		$responseData = $this->buildAssignProjectUser($assignProjectUser,$app);
		return $responseData;
		 
	}
	
	private function buildAssignProjectUser(AssignProjectUser $assignProjectUser, $app)
	
	{
		$data  = array(
				'id' => $assignProjectUser->getIdAssignProjectUser(),
				'fkUser' => $assignProjectUser->getFkUser(),
				'fkProject' => $assignProjectUser->getFkProject()
					);
		return $data;
	}
	
}