<?php

namespace apiv1\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use apiv1\Domain\Project;


class ApiProjectsController {

    /**
     * API project controller.
     *
     * @param Application $app Silex application
     *
     * @return All projects in JSON format
     */
    
    public function getProjects(Application $app)
    
    {
        $projects = $app['dao.project']->findAll();
      //  var_dump($projects);
        // Convert an array of objects ($projects) into an array of associative projects ($responseData)
        $responseData = array();
        foreach ($projects as $project) {
            $responseData[] = $this->buildProjectArrayAllProject($project,$app);
        }
        // Create and return a JSON response
        return $app->json($responseData);
    }
    
    
    /**
     * API project details controller.
     *
     * @param integer $id user id
     * @param Application $app Silex application
     *
     * @return project details in JSON format
     */
    
    public function getProjectByIdUser($id, Application $app) {
    	$projects = $app['dao.project']->findProjectByUser($id);
    	$responseData = $this->buildProjectArray($projects, $app);
    	// Create and return a JSON response
    	return $app->json($responseData);
    }
      
    /**
     * API update project controller with put method.
     *
     * @param integer $id Project id
     * @param Request $request Incoming request
     * @param Application $app Silex application
      
     *
     * @return Project details in JSON format
     */
    
    public function updateProjectById($id,Request $request, Application $app)
     
    {	// Check request parameters
    	$this->checkRequestParameters($request,$app);
    	
    	//find the project by id
    	$project=$project = $app['dao.project']->find($id);
    	
    	//Save the project Object
    	$responseData = $this->saveProjectObject($request, $project, $app);
  
    	return $app->json($responseData, 200);
    
    }
    
    /**
     * API create project controller.
     *
     * @param Request $request Incoming request
     * @param Application $app Silex application
     *
     * @return Project details in JSON format
     */
    public function createProject(Request $request, Application $app) 
    
    {
    	// Check request parameters
    	$this->checkRequestParameters($request,$app);
    	
    	
    	// Build the new project
    	$project = new Project();
    	
    	//Save the project Object
    	$responseData = $this->saveProjectObject($request, $project, $app);
   
    	return $app->json($responseData, 201);
    }
    
    /**
     * API delete project controller.
     *
     * @param integer $id Project id
     * @param Application $app Silex application
     */
    public function deleteProject($id, Application $app)
    
    {
    	// Delete all associated tasks
    	//  $app['dao.taskproject']->deleteAllByArticle($id);
    	// Delete the project
    	$app['dao.project']->delete($id);
    	return $app->json('No Content', 204);  // 204 = No content
    }

//----------------------------------------------------------------------------------------------------------------//
//                                                 Private Function                                               //
//----------------------------------------------------------------------------------------------------------------//
    /**
     * return a list of project into an associative array for JSON encoding
     *
     * @param array A list of all project by user.
     *
     * @return array Associative array whose fields are the task properties.
     */
    //fonction pour project by user
    
    private function buildProjectArray($projects, $app)
    
    {
    	if(empty($projects)){
    		$data=[];
    	}
    	else {
    		foreach ($projects as $project) {
    			$data[]  = array(
    					'id' => $project->getId(),
            			'name' => $project->getName(),
           			//	'client' => $project->getClient(),
    				//	'client' => $app['dao.client']->findNameById($project->getClient()),
    					'client' => $this->buildClientArray($app['dao.client']->findById($project->getClient())),
        				'model' => $project->getModel(),
        				'startDate' => $project->getStartDate(),
        				'endDate' => $project->getEndDate(),
        				'state' => $project->getState(),
        				'creator' => $project->getCreator(),
        			//	'tasks' => $this->buildTaskArray($app['dao.taskproject']->findAllByproject($project->getId())),
    					'categorie' => $this->buildCategoryArray($app['dao.taskproject']->findAllByproject($project->getId()), $app)
    						
    						
    			);
    		}
    	}
    	return $data;
    }
  
    
    
    
    
    /**
     * Converts an Project object into an associative project for JSON encoding
     *
     * @param Project $project Project object
     *
     * @return array Associative project whose fields are the project properties.
     */

//FUNCTION POUR TOUS LES GET ALL PROJECTS
    private function buildProjectArrayAllProject(Project $project, $app) 
    
    {
        $data  = array(
            'id' => $project->getId(),
            'name' => $project->getName(),
        //    'client' => $project->getClient(),
        //	'client' => $app['dao.client']->findNameById($project->getClient()),
        	'client' => $this->buildClientArray($app['dao.client']->findById($project->getClient())),
        	'model' => $project->getModel(),
        	'startDate' => $project->getStartDate(),
        	'endDate' => $project->getEndDate(),
        	'state' => $project->getState(),
        	'creator' => $project->getCreator(),
        //	'tasks' => $this->buildTaskArray($app['dao.taskproject']->findAllByproject($project->getId()))
        	'categorie' => $this->buildCategoryArray($app['dao.taskproject']->findAllByproject($project->getId()), $app)
            );
        return $data;
    }
    
    //categorie array
    function buildCategoryArray($tasks, $app){
    
    	$data = [];
    	foreach($tasks as $task){
    
    		if(!isset($data[$task->getCategory()])){
    			$data[$task->getCategory()] = [];
    		}
    
    		$data[$task->getCategory()][] = [
    				'id' => $task->getId(),
    				'name' => $task->getName(),
    				'description'=>$task->getDescription(),
    				'statut'=>$task->getStatut(),
    				'commentary'=>$task->getCommentary(),
    				'category'=>$task->getCategory(),
    				'creationDate'=>$task->getCreationDate(),
    				'endDate'=>$task->getEndDate(),
    				'fkproject'=>$task->getProject()->getId(),
    				'userType'=>$task->getUserType(),
    				'logHistory'=>$this->buildLogArray($app['dao.logtaskhistory']->findLogByTask($task->getId())),
    				'attachedFile'=>$this->buildFileArray($app['dao.attachedFile']->findFileByTask($task->getId()))
    		];
    	}
    
    	$result = ['categories' => []];
    	foreach($data as $categoryName => $tasks){
    		$tmp = [
    				'categoryName' => $categoryName,
    				'tasks' => $tasks
    		];
    
    
    		$result['categories'][] = $tmp;
    	}
    
    	return $result;
    }
  
    /**
     * return a list of task into an associative array for JSON encoding
     *
     * @param array A list of all tasks for the project.
     *
     * @return array Associative array whose fields are the task properties.
     */
    private function buildTaskArray($tasks) 
    
    {
    	if(empty($tasks)){
    		$data=[];
    	}
    	else {
    		foreach ($tasks as $task) {
    			$data[]  = array(
    					'id' => $task->getId(),
    					'name' => $task->getName(),
    					'description'=>$task->getDescription(),
    					'statut'=>$task->getStatut(),
    					'commentary'=>$task->getCommentary(),
    					'category'=>$task->getCategory(),
    					'creationDate'=>$task->getCreationDate(),
    					'endDate'=>$task->getEndDate(),
    			//		'fkproject'=>$task->getProject()->getId(),
    					'userType'=>$task->getUserType()
    					
    					
    			);
    		}
    	}
    	return $data;
    }
    
    /**
     * return a list of client into an associative array for JSON encoding
     *
     * @param array A list of all client for a projects.
     *
     * @return array Associative array whose fields are the client properties.
     */
    private function buildClientArray($client)
    
    {
   
    			$data  = array(
    					'id' => $client->getId(),
    					'clientName'=>$client->getNameClient(),
    					'clientCode'=>$client->getCodeClient(),
    					//		'idTask'=>$file->getIdTask()
    						
    			); 	
    	return $data;
    }
    
    /**
     * return a list of file into an associative array for JSON encoding
     *
     * @param array A list of all file for a task.
     *
     * @return array Associative array whose fields are the log properties.
     */
    private function buildFileArray($files)
    
    {
    	if(empty($files)){
    		$data=[];
    	}
    	else {
    		foreach ($files as $file) {
    			$data[]  = array(
    					'id' => $file->getId(),
    					'namefile'=>$file->getName(),
    					'namepath'=>$file->getPath(),
    			//		'idTask'=>$file->getIdTask()
    					
    			);
    		}
    	}
    	return $data;
    }
    
    /**
     * return a list of log into an associative array for JSON encoding
     *
     * @param array A list of all log for a task.
     *
     * @return array Associative array whose fields are the log properties.
     */
    private function buildLogArray($logs)
    
    {
    	if(empty($logs)){
    		$data=[];
    	}
    	else {
    		foreach ($logs as $log) {
    			$data[]  = array(
    					'id' => $log->getId(),
   	 					'fktask'=>$log->getfkTask(),
    					'date'=>$log->getDate(),
    					'userTrigramme'=>$log->getUserTrigramme(),
    					'typeLog'=>$log->getTypeLog(),
    					'commentary'=>$log->getCommentary()
    			);
    		}
    	}
    	return $data;
    }
    
    /**
     * check the request parameters
     *
     *@param Request $request Incoming request
     *@param Application $app Silex application
     *
     * @return an json with code 400 if the parameters are false.
     */
    
    private function checkRequestParameters(Request $request, Application $app) 
    {
    	// Check request parameters
    	if (!$request->request->has('name')) {
    		return $app->json('Missing required parameter: title', 400);
    	}
    	if (!$request->request->has('client')) {
    		return $app->json('Missing required parameter: client', 400);
    	}
    	if (!$request->request->has('model')) {
    		return $app->json('Missing required parameter: model', 400);
    	}
    	if (!$request->request->has('startDate')) {
    		return $app->json('Missing required parameter: startDate', 400);
    	}
    	if (!$request->request->has('endDate')) {
    		return $app->json('Missing required parameter: endDate', 400);
    	}
    	if (!$request->request->has('state')) {
    		return $app->json('Missing required parameter: state', 400);
    	}
    	if (!$request->request->has('creator')) {
    		return $app->json('Missing required parameter: creator', 400);
    	}
    }
    
    /**
     * save the project
     *
     *@param Request $request Incoming request
     *@param Projet $project 
     *@param Application $app
     *
     * @return an array .
     */
    private function saveProjectObject(Request $request, Project $project, Application $app)
    {
    	$project->setName($request->request->get('name'));
    	$project->setClient($request->request->get('client'));
    	$project->setModel($request->request->get('model'));
    	$project->setStartDate($request->request->get('startDate'));
    	$project->setEndDate($request->request->get('endDate'));
    	$project->setState($request->request->get('state'));
    	$project->setCreator($request->request->get('creator'));
    	
    	$app['dao.project']->save($project);
    	$responseData = $this->buildProjectArrayAllProject($project,$app);
    //	$responseData = $this->buildProjectArray($project,$app);
    	return $responseData;
    	
    }
}
