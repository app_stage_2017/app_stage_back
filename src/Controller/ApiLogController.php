<?php

namespace apiv1\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use apiv1\Domain\LogTaskHistory;


class ApiLogController {
	
	/**
	 * API log details controller.
	 *
	 * @param integer $id task id
	 * @param Application $app Silex application
	 *
	 * @return log details in JSON format
	 */
	
	public function getLogByIdTask($id, Application $app) {
		$logs = $app['dao.logtaskhistory']->findAllLogByTask($id);
		$responseData = $this->buildLogArray($logs);
		// Create and return a JSON response
		return $app->json($responseData);
	}
	
	/**
	 * API create log controller.
	 *
	 * @param Request $request Incoming request
	 * @param Application $app Silex application
	 *
	 * @return log details in JSON format
	 */
	public function createLog(Request $request, Application $app)
	
	{
		// Check request parameters
		$this->checkRequestParameters($request,$app);
		 
		 
		// Build the new log
		$log = new LogTaskHistory();
		 
		//Save the project Object
		$responseData = $this->saveLogObject($request, $log, $app);
		 
		return $app->json($responseData, 201);
	}
	
	
	
	/**
	 * return a list of log into an associative array for JSON encoding
	 *
	 * @param array A list of all log for a task.
	 *
	 * @return array Associative array whose fields are the log properties.
	 */
	private function buildLogArray($logs)
	
	{
		if(empty($logs)){
			$data=[];
		}
		else {
			foreach ($logs as $log) {
				$data[]  = array(
						'id' => $log->getId(),
						'fkTask'=>$log->getfkTask(),
						'date'=>$log->getDate(),
						'userTrigramme'=>$log->getUserTrigramme(),
						'typeLog'=>$log->getTypeLog(),
						'commentary'=>$log->getCommentary()
				);
			}
		}
		return $data;
	}
	
	/**
	 * return a list of log into an associative array for JSON encoding
	 *
	 * @param array A list of all log for a task.
	 *
	 * @return array Associative array whose fields are the log properties.
	 */
	private function buildLogArray2($log)
	
	{
		$data  = array(
				'id' => $log->getId(),
				'fkTask'=>$log->getfkTask(),
				'date'=>$log->getDate(),
				'userTrigramme'=>$log->getUserTrigramme(),
				'typeLog'=>$log->getTypeLog(),
				'commentary'=>$log->getCommentary()
				
	
		);
		return $data;
	}
	
	
	/**
	 * check the request parameters
	 *
	 *@param Request $request Incoming request
	 *@param Application $app Silex application
	 *
	 * @return an json with code 400 if the parameters are false.
	 */
	
	private function checkRequestParameters(Request $request, Application $app)
	{
		// Check request parameters
		if (!$request->request->has('fktask')) {
			return $app->json('Missing required parameter: associatedTask', 400);
		}
		if (!$request->request->has('date')) {
			return $app->json('Missing required parameter: date', 400);
		}
		if (!$request->request->has('userTrigramme')) {
			return $app->json('Missing required parameter: userTrigramme', 400);
		}
		if (!$request->request->has('typeLog')) {
			return $app->json('Missing required parameter: typeLog', 400);
		}
		
	}
	
	/**
	 * save the log
	 *
	 *@param Request $request Incoming request
	 *@param Log $log
	 *@param Application $app
	 *
	 * @return an array .
	 */
	private function saveLogObject(Request $request, LogTaskHistory $log, Application $app)
	{
		$log->setDate($request->request->get('date'));
		$log->setUserTrigramme($request->request->get('userTrigramme'));
		$log->setTypeLog($request->request->get('typeLog'));
		$log->setfkTask($request->request->get('fktask'));
		$log->setCommentary($request->request->get('commentary'));
		
		$app['dao.logtaskhistory']->save($log);
		$responseData = $this->buildLogArray2($log);
		return $responseData;
		 
	}
	
	
}