<?php

namespace apiv1\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use apiv1\Domain\Client;




class ApiClientController {

//----------------------------------------------------------------------------------------------------------------//
//                                                 Clients Controller                                              //
//----------------------------------------------------------------------------------------------------------------//
	
	
	
    /**
     * API client controller.
     *
     * @param Application $app Silex application
     *
     * @return All client in JSON format
     */
    
    public function getClients(Application $app)
    
    {
        $clients = $app['dao.client']->findAll();
        // Convert an array of objects ($clients) into an array of associative projects ($responseData)
        $responseData = array();
        foreach ($clients as $client) {
            $responseData[] = $this->buildClientArray($client,$app);
        }
        // Create and return a JSON response
        return $app->json($responseData);
    }
    
    /**
     * API create client controller.
     *
     * @param Request $request Incoming request
     * @param Application $app Silex application
     *
     * @return Client details in JSON format
     */
    public function createClient(Request $request, Application $app)
    
    {
    	// Check request parameters
    	if (!$request->request->has('clientName')) {
    		return $app->json('Missing required parameter: name', 400);
    	}
    	if (!$request->request->has('clientCode')) {
    		return $app->json('Missing required parameter: code', 400);
    	}
    	
    	 
    	// Build the new client
    	$client = new Client();
    
    	//Save the client Object
    	$client->setNameClient($request->request->get('clientName'));
    	$client->setCodeClient($request->request->get('clientCode'));
    	$app['dao.client']->save($client);
    	$responseData = $this->buildClientArray($client,$app);
    
    	return $app->json($responseData, 201);
    }
    
    /**
     * API delete client controller.
     *
     * @param integer $id Client id
     * @param Application $app Silex application
     */
    public function deleteClient($id, Application $app)
    
    {
    	// Delete the client
    	$app['dao.client']->delete($id);
    	return $app->json('No Content', 204);  // 204 = No content
    }
    
    /**
     * Converts an Client object into an associative category for JSON encoding
     *
     * @param Client $client Client object
     *
     * @return array Associative client whose fields are the pcategory properties.
     */
    
    
    private function buildClientArray(Client $client, $app)
    
    {
    	$data  = array(
    			'id' => $client->getId(),
    			'name' => $client->getNameClient(),
    			'code'=> $client->getCodeClient(),
    	);
    	return $data;
    }
}    