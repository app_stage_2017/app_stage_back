<?php 

namespace apiv1\DAO;

use apiv1\Domain\Client;


class ClientDAO extends DAO 
{
	
	/**
	 * Return a list of all client, sorted by name (alpha).
	 *
	 * @return array A list of all client.
	 */
	
	public function findAll() {
		$sql = "select * from client order by idClient asc";
		$result = $this->getDb()->fetchAll($sql);
	
		// Convert query result to an array of domain objects
		$clients = array();
		foreach ($result as $row) {
			$clientId = $row['idClient'];
			$clients[$clientId] = $this->buildDomainObject($row);
		}
		return $clients;
	}



	/**
	 * Returns an client matching the supplied id.
	 *
	 * @param integer $id
	 *
	 * @return \apiv1\Domain\Client|throws an exception if no matching article is found
	 */
	
	public function findById($id) {
		$sql = "select * from client where idClient=?";
		$row = $this->getDb()->fetchAssoc($sql, array($id));
	
		if ($row)
			return $this->buildDomainObject($row);
			else
				throw new \Exception("No client matching id " . $id);
	}
	
	/**
	 * Returns an clientName matching the supplied id.
	 *
	 * @param integer $id
	 *
	 * @return \apiv1\Domain\Client|throws an exception if no matching article is found
	 */
	
	public function findNameById($id) {
		$sql = "select clientName from client where idClient=?";
		$row = $this->getDb()->fetchAssoc($sql, array($id));
		
	
		if ($row)
		{
			$client = new Client();
			$client->setNameClient($row['clientName']);
			return $client->getNameClient();
		//	return $this->buildDomainObject($row);
		}
			else
				throw new \Exception("No client matching id " . $id);
				
	}
	
	
	/**
	 * Creates an Model object based on a DB row.
	 *
	 * @param array $row The DB row containing Model data.
	 * @return \apiv1\Domain\Model
	 */
			
	protected function buildDomainObject(array $row) {
		$client = new Client();
		$client->setId($row['idClient']);
		$client->setNameClient($row['clientName']);
		$client->setCodeClient($row['clientCode']);
		return $client;
	}
	
	/**
	 * Saves an client into the database.
	 *
	 * @param \apiv1\Domain\Client $client The client to save
	 */
			
	public function save(Client $client) {
		$clientData = array(
				'clientName' => $client->getNameClient(),
				'clientCode'=>$client->getCodeClient(),
		);
	
		if ($client->getId()) {
			// The client has already been saved : update it
			$this->getDb()->update('client', $clientData, array('idClient' => $client->getId()));
		} else {
			// The client has never been saved : insert it
			$this->getDb()->insert('client', $clientData);
			// Get the id of the newly created project and set it on the entity.
			$id = $this->getDb()->lastInsertId();
			$client->setId($id);
		}
	}
	
	/**
	 * Removes an client from the database.
	 *
	 * @param integer $id The client id.
	 */
			
	public function delete($id) {
		// Delete the client
		$this->getDb()->delete('client', array('idClient' => $id));
	}
	
	
	
	
	
	
	
	
	
	
	
}



