<?php 

namespace apiv1\DAO;


use apiv1\Domain\Category;

class CategoryDAO extends DAO 

{
	/**
	 * @var \apiv1\DAO\ModelDAO
	 *
	 * use in modelDAO
	 */
	private $modelDAO;
	
	
	public function setModelDAO(ModelDAO $modelDAO) {
		$this->modelDAO = $modelDAO;
	}
	
	public function findAll() {
		$sql = "SELECT category.* FROM model 
				JOIN linkModelCategory ON (model.idModel = linkModelCategory.fkModel) 
				JOIN category ON (category.idCategory = linkModelCategory.fkCategory)";
		$result = $this->getDb()->fetchAll($sql);
	/*	SELECT category.* FROM model JOIN linkModelCategory ON (model.idModel = linkModelCategory.fkModelProject) JOIN category ON (category.idCategory = linkModelCategory.fkCategory) WHERE model.idModel = 1 */
	/* SELECT category.categoryName FROM model JOIN linkModelCategory ON (model.idModel = linkModelCategory.fkModelProject) JOIN category ON (category.idCategory = linkModelCategory.fkCategory) */
	/*SELECT model.modelName,category.categoryName FROM model JOIN linkModelCategory ON (model.idModel = linkModelCategory.fkModelProject) JOIN category ON (category.idCategory = linkModelCategory.fkCategory) */	
		// Convert query result to an array of domain objects
		
		$categories = array();
		foreach ($result as $row) {
			$categoryId = $row['idCategory'];
			$categories[$categoryId] = $this->buildDomainObject($row);
		}
		return $categories;
	}
	
	public function findCategoryByModel($modelId) {
		// The associated model is retrieved only once
		$model = $this->modelDAO->find($modelId);
		
		$sql = "SELECT category.* FROM model
				JOIN linkModelCategory ON (model.idModel = linkModelCategory.fkModel)
				JOIN category ON (category.idCategory = linkModelCategory.fkCategory)
				WHERE model.idModel=?";
		
		$result = $this->getDb()->fetchAll($sql,array($modelId));
		
		$categories = array();
		foreach ($result as $row) {
			$categoryId = $row['idCategory'];
			$category=$this->buildDomainObject($row);
			$category->setModel($model);
			$categories[$categoryId] = $category;
		}
		return $categories;	
	}
	
	/**
	 * Returns an category matching the supplied id.
	 *
	 * @param integer $id
	 *
	 * @return \apiv1\Domain\Category|throws an exception if no matching category is found
	 */
	public function find($id) {
		$sql = "select * from category where idCategory=?";
		$row = $this->getDb()->fetchAssoc($sql, array($id));
	
		if ($row)
			return $this->buildDomainObject($row);
			else
				throw new \Exception("No category matching id " . $id);
	}

	
	/**
	 * Creates an Category object based on a DB row.
	 *
	 * @param array $row The DB row containing Category data.
	 * @return \apiv1\Domain\Category
	 */
	protected function buildDomainObject(array $row) {
		$category = new Category();
		$category->setId($row['idCategory']);
		$category->setName($row['categoryName']);
		return $category;
	}
	
	/**
	 * Saves an category into the database.
	 *
	 * @param \apiv1\Domain\Category $category The category to save
	 */
	public function save(Category $category) {
		$categoryData = array(
				'categoryName' => $category->getName()
		);
	
		if ($category->getId()) {
			// The category has already been saved : update it
			$this->getDb()->update('category', $categoryData, array('idCategory' => $category->getId()));
		} else {
			// The project has never been saved : insert it
			$this->getDb()->insert('category', $categoryData);
			// Get the id of the newly created project and set it on the entity.
			$id = $this->getDb()->lastInsertId();
			$category->setId($id);
		}
	}
	

	
	/**
	 * Removes an category from the database.
	 *
	 * @param integer $id The category id.
	 */
	public function delete($id) {
		// Delete the category
		$this->getDb()->delete('category', array('idCategory' => $id));
	}
	
	
	
	
}	