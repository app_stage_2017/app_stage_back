<?php 

namespace apiv1\DAO;

use apiv1\Domain\Model;


class ModelDAO extends DAO 
{
	
	/**
	 * Return a list of all model, sorted by name (alpha).
	 *
	 * @return array A list of all model.
	 */
	
	public function findAll() {
		$sql = "select * from model order by idModel asc";
		$result = $this->getDb()->fetchAll($sql);
	
		// Convert query result to an array of domain objects
		$models = array();
		foreach ($result as $row) {
			$modelId = $row['idModel'];
			$models[$modelId] = $this->buildDomainObject($row);
		}
		return $models;
	}
	
	/**
	 * Returns an model matching the supplied id.
	 *
	 * @param integer $id
	 *
	 * @return \apiv1\Domain\Model|throws an exception if no matching article is found
	 */
	public function find($id) {
		$sql = "select * from model where idModel=?";
		$row = $this->getDb()->fetchAssoc($sql, array($id));
	
		if ($row)
			return $this->buildDomainObject($row);
			else
				throw new \Exception("No model matching id " . $id);
	}
	
	
	/**
	 * Creates an Model object based on a DB row.
	 *
	 * @param array $row The DB row containing Model data.
	 * @return \apiv1\Domain\Model
	 */
	protected function buildDomainObject(array $row) {
		$model = new Model();
		$model->setId($row['idModel']);
		$model->setName($row['modelName']);
		return $model;
	}
	
	/**
	 * Saves an model into the database.
	 *
	 * @param \apiv1\Domain\Model $model The model to save
	 */
	public function save(Model $model) {
		$modelData = array(
				'modelName' => $model->getName()
		);
	
		if ($model->getId()) {
			// The model has already been saved : update it
			$this->getDb()->update('model', $modelData, array('idModel' => $model->getId()));
		} else {
			// The model has never been saved : insert it
			$this->getDb()->insert('model', $modelData);
			// Get the id of the newly created model and set it on the entity.
			$id = $this->getDb()->lastInsertId();
			$model->setId($id);
		}
	}
	
	/**
	 * Removes an model from the database.
	 *
	 * @param integer $id The model id.
	 */
	public function delete($id) {
		// Delete the model
		$this->getDb()->delete('model', array('idModel' => $id));
	}
	
	
	
	
	
	
	
	
	
	
	
}



