<?php

namespace apiv1\DAO;

use apiv1\Domain\ModelAttachedFile;

class ModelAttachedFileDAO extends DAO

{
	/**
	 * @var \apiv1\DAO\TaskDAO
	 *
	 * use in taskDAO
	 */
	private $taskDAO;
	
	public function setTaskDAO(TaskDAO $taskDAO) {
		$this->taskDAO = $taskDAO;
	}
	
	
	
	/**
	 * Return a list of all file for an taskProject, sorted by id ).
	 *
	 * @param integer $taskProjectId The taskProject id.
	 *
	 * @return array A list of all file for the taskproject.
	 */
	public function findFileByTask($taskId) {
		// The associated task is retrieved only once
	//	$task = $this->taskProjectDAO->find($taskId);
	
		
		// The attachedFile won't be retrieved during domain objet construction
		$sql = "select * from modelattachedFile where modelattachedFileIdTak=? order by idModelAttachedFile";
	
		$result = $this->getDb()->fetchAll($sql, array($taskId));
	
		// Convert query result to an array of domain objects
		$files = array();
		foreach ($result as $row) {
			$fileId = $row['idModelAttachedFile'];
			$file = $this->buildDomainObject($row);
			// The associated article is defined for the constructed comment
		//	$file->setTaskProjectDAO($task);
			$files[$fileId] = $file;
		}
		return $files;
	}
	
	/**
	 * Saves an file into the database.
	 *
	 * @param \apiv1\Domain\ModelAttachedFile $file The file to save
	 */
	
	public function save(ModelAttachedFile $file) {
		$fileData = array(
				'modelattachedFileName' => $file->getName(),
				'modelattachedFilePath' => $file->getPath(),
				'modelattachedFileIdTak' => $file->getIdTask(),
				
				 
		);
	
		if ($file->getId()) {
			// The file has already been saved : update it
			$this->getDb()->update('modelattachedFile', $fileData, array('idModelAttachedFile' => $file->getId()));
		} else {
			// The file has never been saved : insert it
			$this->getDb()->insert('modelattachedFile', $fileData);
			// Get the id of the newly created file and set it on the entity.
			$id = $this->getDb()->lastInsertId();
			$file->setId($id);
		}
	}
	
	/**
	 * Removes an file from the database.
	 *
	 * @param integer $id The File id.
	 */
	
	public function delete($id) {
		// Delete the file
		$this->getDb()->delete('modelattachedFile', array('idModelAttachedFile' => $id));
	}
	
	/**
	 * Creates an File object based on a DB row.
	 *
	 * @param array $row The DB row containing File data.
	 * @return \apiv1\Domain\AttachedFile
	 */
	protected function buildDomainObject(array $row) {
		$file = new ModelAttachedFile();
		$file->setId($row['idModelAttachedFile']);
		$file->setName($row['modelAttachedFileName']);
		$file->setPath($row['modelAttachedFilePath']);
	//	$file->setIdTask($row['attachedFileIdTak']);
	
	/*	
		if (array_key_exists('attachedFileIdTak', $row)) {
			// Find and set the associated task
			$taskId = $row['attachedFileIdTak'];
			$task = $this->taskProjectDAO->find($taskId);
			$file->setIdTask($task);
		}
		*/
	
		return $file;
	}
	
	
	
}
