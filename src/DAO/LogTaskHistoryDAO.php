<?php

namespace apiv1\DAO;

use apiv1\Domain\LogTaskHistory;

class LogTaskHistoryDAO extends DAO 
{

	
	
	/**
	 * Return an log for an taskProject, sorted by date ).
	 *
	 * @param integer $taskId The task id.
	 *
	 * @return array A list of all logs for the task.
	 */
	public function findLogByTask($taskId) {

		$sql = "select * from logTaskHistory where fkTask=? order by dateLogTask DESC LIMIT 1";
	
		$result = $this->getDb()->fetchAll($sql, array($taskId));
	
		// Convert query result to an array of domain objects
		$logs = array();
		foreach ($result as $row) {
			$logId = $row['idLogTaskHistory'];
			$log = $this->buildDomainObject($row);
			$logs[$logId] = $log;
		}
		return $logs;
	}
	
	/**
	 * Return a list of all log for an taskProject, sorted by date ).
	 *
	 * @param integer $taskId The task id.
	 *
	 * @return array A list of all logs for the task.
	 */
	public function findAllLogByTask($taskId) {
	
		$sql = "select * from logTaskHistory where fkTask=? order by dateLogTask DESC";
	
		$result = $this->getDb()->fetchAll($sql, array($taskId));
	
		// Convert query result to an array of domain objects
		$logs = array();
		foreach ($result as $row) {
			$logId = $row['idLogTaskHistory'];
			$log = $this->buildDomainObject($row);
			$logs[$logId] = $log;
		}
		return $logs;
	}
	
	/**
	 * Saves an log into the database.
	 *
	 * @param \apiv1\Domain\LogTaskHistory $task The log to save
	 */
	public function save(LogTaskHistory $log) {
		$logData = array(
				'fkTask' => $log->getfkTask(),
				'dateLogTask' => $log->getDate(),
				'userTrigramme' => $log->getUserTrigramme(),
				'typeLog'=>$log->getTypeLog(),
				'commentary'=>$log->getCommentary(),
				
		);
	
		if ($log->getId()) {
			// The log has already been saved : update it
			$this->getDb()->update('logTaskHistory', $logData, array('idLogTaskHistory' => $log->getId()));
		} else {
			// The log has never been saved : insert it
			$this->getDb()->insert('logTaskHistory', $logData);
			// Get the id of the newly created log and set it on the entity.
			$id = $this->getDb()->lastInsertId();
			$log->setId($id);
		}
	}
	
	
	
	
	/**
	 * Creates an Log object based on a DB row.
	 *
	 * @param array $row The DB row containing Log data.
	 * @return \apiv1\Domain\LogTaskHistory
	 */
	protected function buildDomainObject(array $row) {
		$log = new LogTaskHistory();
		$log->setId($row['idLogTaskHistory']);
		$log->setDate($row['dateLogTask']);
		$log->setUserTrigramme($row['userTrigramme']);
		$log->setTypeLog($row['typeLog']);
		$log->setfkTask($row['fkTask']);
		$log->setCommentary($row['commentary']);

		return $log;
	}

}