<?php

namespace apiv1\DAO;

use apiv1\Domain\Task;

class TaskDAO extends DAO 
{
    /**
     * @var \apiv1\DAO\CategoryDAO
     * 
     * use in categoryDAO
     */
    private $categoryDAO;
    
    public function setCategoryDAO(CategoryDAO $categoryDAO) {
        $this->categoryDAO = $categoryDAO;
    }
    
    /**
     * Return a list of all tasks, sorted by date (most recent first).
     *
     * @return array A list of all tasks.
     */
/*    
    public function findAll() {
    	$sql = "select * from task";
    	$result = $this->getDb()->fetchAll($sql);
    
    	// Convert query result to an array of domain objects
    	$tasks = array();
    	foreach ($result as $row) {
    		$taskId = $row['idTaskProject'];
    		$tasks[$taskId] = $this->buildDomainObject($row);
    	}
    	return $tasks;
    }
*/
    /**
     * Return a list of all tasks for an category, sorted by id ).
     *
     * @param integer $categoryId The category id.
     *
     * @return array A list of all tasks for the category.
     */
    public function findTaskByCategory($categoryId) {
        // The associated project is retrieved only once
        $category = $this->categoryDAO->find($categoryId);

        // idTask is not selected by the SQL query
        // The category won't be retrieved during domain objet construction
        $sql = "select * from task where fk_category=? order by idTask";

        $result = $this->getDb()->fetchAll($sql, array($categoryId));

        // Convert query result to an array of domain objects
        $tasks = array();
        foreach ($result as $row) {
            $taskId = $row['idTask'];
            $task = $this->buildDomainObject($row);
            // The associated article is defined for the constructed comment
            $task->setCategory($category);
            $tasks[$taskId] = $task;
        }
        return $tasks;
    }
    
    /**
     * Returns an task matching the supplied id.
     *
     * @param integer $id
     *
     * @return \apiv1\Domain\TaskProject|throws an exception if no matching article is found
     */
 /* 
    public function find($id) {
    	$sql = "select * from taskProject where idTaskProject=?";
    	$row = $this->getDb()->fetchAssoc($sql, array($id));
    
    	if ($row)
    		return $this->buildDomainObject($row);
    		else
    			throw new \Exception("No article matching id " . $id);
    }
 */   
    
    /**
     * Saves an task into the database.
     *
     * @param \apiv1\Domain\Task $task The task to save
     */
    		
    public function save(Task $task) {
    	$taskData = array(
    			'taskName' => $task->getName(),
    			'taskDescription' => $task->getDescription(),
    			//'taskProjectStatut' => $task->getStatut(),
    			'fk_category' => $task->getCategory(),
    			'categoryTask'=>$task->getCategoryTask(),
    			'userType'=>$task->getUserType()
    			
    	);
    
    	if ($task->getId()) {
    		// The project has already been saved : update it
    		$this->getDb()->update('task', $taskData, array('idTask' => $task->getId()));
    	} else {
    		// The project has never been saved : insert it
    		$this->getDb()->insert('task', $taskData);
    		// Get the id of the newly created project and set it on the entity.
    		$id = $this->getDb()->lastInsertId();
    		$task->setId($id);
    	}
    }
  
    /**
     * Removes an task from the database.
     *
     * @param integer $id The Task id.
     */
    
    public function delete($id) {
    	// Delete the task
    	$this->getDb()->delete('task', array('idTask' => $id));
    }

    /**
     * Creates an Task object based on a DB row.
     *
     * @param array $row The DB row containing Task data.
     * @return \apiv1\Domain\TaskProject
     */
    protected function buildDomainObject(array $row) {
        $task = new Task();
        $task->setId($row['idTask']);
        $task->setName($row['taskName']);
        $task->setDescription($row['taskDescription']);
        $task->setCategoryTask($row['categoryTask']);
        $task->setUserType($row['userType']);

        if (array_key_exists('fk_category', $row)) {
            // Find and set the associated project
            $categoryId = $row['fk_category'];
            $category = $this->categoryDAO->find($categoryId);
            $task->setCategory($category);
        }
        
        return $task;
    }
}