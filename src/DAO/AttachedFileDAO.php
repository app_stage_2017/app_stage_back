<?php

namespace apiv1\DAO;

use apiv1\Domain\AttachedFile;

class AttachedFileDAO extends DAO

{
	/**
	 * @var \apiv1\DAO\TaskProjectDAODAO
	 *
	 * use in taskProjectDAO
	 */
	private $taskProjectDAO;
	
	public function setTaskProjectDAO(TaskProjectDAO $taskProjectDAO) {
		$this->taskProjectDAO = $taskProjectDAO;
	}
	
	
	
	/**
	 * Return a list of all file for an taskProject, sorted by id ).
	 *
	 * @param integer $taskProjectId The taskProject id.
	 *
	 * @return array A list of all file for the taskproject.
	 */
	public function findFileByTask($taskId) {
		// The associated task is retrieved only once
	//	$task = $this->taskProjectDAO->find($taskId);
	
		
		// The attachedFile won't be retrieved during domain objet construction
		$sql = "select * from attachedFile where attachedFileIdTak=? order by idAttachedFile";
	
		$result = $this->getDb()->fetchAll($sql, array($taskId));
	
		// Convert query result to an array of domain objects
		$files = array();
		foreach ($result as $row) {
			$fileId = $row['idAttachedFile'];
			$file = $this->buildDomainObject($row);
			// The associated article is defined for the constructed comment
		//	$file->setTaskProjectDAO($task);
			$files[$fileId] = $file;
		}
		return $files;
	}
	
	/**
	 * Saves an file into the database.
	 *
	 * @param \apiv1\Domain\AttachedFile $file The file to save
	 */
	
	public function save(AttachedFile $file) {
		$fileData = array(
				'attachedFileName' => $file->getName(),
				'attachedFilePath' => $file->getPath(),
				'attachedFileIdTak' => $file->getIdTask(),
				
				 
		);
	
		if ($file->getId()) {
			// The file has already been saved : update it
			$this->getDb()->update('attachedFile', $fileData, array('idAttachedFile' => $file->getId()));
		} else {
			// The file has never been saved : insert it
			$this->getDb()->insert('attachedFile', $fileData);
			// Get the id of the newly created file and set it on the entity.
			$id = $this->getDb()->lastInsertId();
			$file->setId($id);
		}
	}
	
	/**
	 * Removes an file from the database.
	 *
	 * @param integer $id The File id.
	 */
	
	public function delete($id) {
		// Delete the file
		$this->getDb()->delete('attachedFile', array('idAttachedFile' => $id));
	}
	
	/**
	 * Creates an File object based on a DB row.
	 *
	 * @param array $row The DB row containing File data.
	 * @return \apiv1\Domain\AttachedFile
	 */
	protected function buildDomainObject(array $row) {
		$file = new AttachedFile();
		$file->setId($row['idAttachedFile']);
		$file->setName($row['attachedFileName']);
		$file->setPath($row['attachedFilePath']);
	//	$file->setIdTask($row['attachedFileIdTak']);
	
	/*	
		if (array_key_exists('attachedFileIdTak', $row)) {
			// Find and set the associated task
			$taskId = $row['attachedFileIdTak'];
			$task = $this->taskProjectDAO->find($taskId);
			$file->setIdTask($task);
		}
		*/
	
		return $file;
	}
	
	
	
}