<?php

namespace apiv1\DAO;

use apiv1\Domain\TaskProject;

class TaskProjectDAO extends DAO 
{
    /**
     * @var \apiv1\DAO\ProjectDAO
     * 
     * use in projectDAO
     */
    private $projectDAO;
    
    public function setProjectDAO(ProjectDAO $projectDAO) {
        $this->projectDAO = $projectDAO;
    }
    
    /**
     * Return a list of all tasks, sorted by date (most recent first).
     *
     * @return array A list of all tasks.
     */
    public function findAll() {
    	$sql = "select * from taskProject order by taskCreationDate desc";
    	$result = $this->getDb()->fetchAll($sql);
    
    	// Convert query result to an array of domain objects
    	$tasks = array();
    	foreach ($result as $row) {
    		$taskId = $row['idTaskProject'];
    		$tasks[$taskId] = $this->buildDomainObject($row);
    	}
    	return $tasks;
    }

    /**
     * Return a list of all tasks for an project, sorted by date (most recent last).
     *
     * @param integer $projectId The project id.
     *
     * @return array A list of all tasks for the project.
     */
    public function findAllByProject($projectId) {
        // The associated project is retrieved only once
        $project = $this->projectDAO->find($projectId);

        // idTaskProject is not selected by the SQL query
        // The project won't be retrieved during domain objet construction
        $sql = "select * from taskProject where fkproject=? order by idTaskProject";
        $result = $this->getDb()->fetchAll($sql, array($projectId));

        // Convert query result to an array of domain objects
        $tasks = array();
        foreach ($result as $row) {
            $taskId = $row['idTaskProject'];
            $task = $this->buildDomainObject($row);
            // The associated article is defined for the constructed comment
            $task->setProject($project);
            $tasks[$taskId] = $task;
        }
        return $tasks;
    }
    
    /**
     * Returns an task matching the supplied id.
     *
     * @param integer $id
     *
     * @return \apiv1\Domain\TaskProject|throws an exception if no matching article is found
     */
    public function find($id) {
    	$sql = "select * from taskProject where idTaskProject=?";
    	$row = $this->getDb()->fetchAssoc($sql, array($id));
    
    	if ($row)
    		return $this->buildDomainObject($row);
    		else
    			throw new \Exception("No article matching id " . $id);
    }
    
    
    /**
     * Saves an task into the database.
     *
     * @param \apiv1\Domain\TaskProject $task The task to save
     */
    public function save(TaskProject $task) {
    	$taskData = array(
    			'taskProjectName' => $task->getName(),
    			'taskProjectDescirption' => $task->getDescription(),
    			'taskProjectStatut' => $task->getStatut(),
    			'taskProjectCommentary'=>$task->getCommentary(),
    			'taskProjectCategoryName' => $task->getCategory(),
    			'taskCreationDate' => $task->getCreationDate(),
    			'taskEndDate' => $task->getEndDate(),
    			'fkproject' => $task->getProject()->getId(),
    			'taskUserType' => $task->getUserType()
    	);
    
    	if ($task->getId()) {
    		// The project has already been saved : update it
    		$this->getDb()->update('taskProject', $taskData, array('idTaskProject' => $task->getId()));
    	} else {
    		// The project has never been saved : insert it
    		$this->getDb()->insert('taskProject', $taskData);
    		// Get the id of the newly created project and set it on the entity.
    		$id = $this->getDb()->lastInsertId();
    		$task->setId($id);
    	}
    }
    
    /**
     * Removes an task from the database.
     *
     * @param integer $id The TaskProject id.
     */
    public function delete($id) {
    	// Delete the task
    	$this->getDb()->delete('taskProject', array('idTaskProject' => $id));
    }

    /**
     * Creates an Task object based on a DB row.
     *
     * @param array $row The DB row containing Task data.
     * @return \apiv1\Domain\TaskProject
     */
    protected function buildDomainObject(array $row) {
        $task = new TaskProject();
        $task->setId($row['idTaskProject']);
        $task->setName($row['taskProjectName']);
        $task->setDescription($row['taskProjectDescirption']);
        $task->setStatut($row['taskProjectStatut']);
        $task->setCommentary($row['taskProjectCommentary']);
        $task->setCategory($row['taskProjectCategoryName']);
        $task->setCreationDate($row['taskCreationDate']);
        $task->setEndDate($row['taskEndDate']);
        $task->setUserType($row['taskUserType']);

        if (array_key_exists('fkproject', $row)) {
            // Find and set the associated project
            $projectId = $row['fkproject'];
            $project = $this->projectDAO->find($projectId);
            $task->setProject($project);
        }
        
        return $task;
    }
}