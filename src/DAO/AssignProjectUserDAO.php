<?php 

namespace apiv1\DAO;

use apiv1\Domain\AssignProjectUser;


class AssignProjectUserDAO extends DAO 
{
	/**
	 * Creates an AssignProjectUser object based on a DB row.
	 *
	 * @param array $row The DB row containing AssignProjectUser data.
	 * @return \apiv1\Domain\AssignProjectUser
	 */
	protected function buildDomainObject(array $row) {
		
		$assignProjectUser = new AssignProjectUserDAO();
		$assignProjectUser->setIdAssignProjectUser($row['idTask']);
		$assignProjectUser->setFkProject($row['fkProject']);
		$assignProjectUser->setFkUser($row['fkUser']);
		return $assignProjectUser;
	}
	
	/**
	 * Saves an assignprojectuser into the database.
	 *
	 * @param \apiv1\Domain\AssignProjectUser $assignprojectuser The assignprojectuser to save
	 */
	public function save(AssignProjectUser $assignprojectuser) {
		$assignprojectuserData = array(
				'fkProject' => $assignprojectuser->getFkProject(),
				'fkUser' => $assignprojectuser->getFkUser()
				
		);
		if ($assignprojectuser->getIdAssignProjectUser()) {
			// The assignprojectuser has already been saved : update it
			$this->getDb()->update('assignprojectuser', $assignprojectuserData, array('idAssignProjectUser' => $assignprojectuser->getIdAssignProjectUser()));
		} else {
			// The assignprojectuser has never been saved : insert it
			$this->getDb()->insert('assignprojectuser', $assignprojectuserData);
			// Get the id of the newly created project and set it on the entity.
			$id = $this->getDb()->lastInsertId();
			$assignprojectuser->setIdAssignProjectUser($id);
		}
	}
	
	/**
	 * Removes an assignprojectuser from the database.
	 *
	 * @param integer $id The idAssignProjectUser id.
	 */
	public function delete($id) {
		// Delete the article
		$this->getDb()->delete('assignprojectuser', array('idAssignProjectUser' => $id));
	}

}