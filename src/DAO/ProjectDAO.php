<?php

namespace apiv1\DAO;

use apiv1\Domain\Project;


class ProjectDAO extends DAO
{
	/**
	 * @var \apiv1\DAO\UserDAO
	 *
	 * use in userDAO
	 */
	private $userDAO;
	
	
	public function setUserDAO(UserDAO $userDAO) {
		$this->userDAO = $userDAO;
	}
	
	public function findAll() {
		$sql = "SELECT project.* FROM user
				JOIN assignProjectUser ON (user.idUser = assignProjectUser.fkUser)
				JOIN project ON (project.idProject = assignProjectUser.fkProject)";
		$result = $this->getDb()->fetchAll($sql);
		/*	SELECT category.* FROM model JOIN linkModelCategory ON (model.idModel = linkModelCategory.fkModelProject) JOIN category ON (category.idCategory = linkModelCategory.fkCategory) WHERE model.idModel = 1 */
		/* SELECT category.categoryName FROM model JOIN linkModelCategory ON (model.idModel = linkModelCategory.fkModelProject) JOIN category ON (category.idCategory = linkModelCategory.fkCategory) */
		/*SELECT model.modelName,category.categoryName FROM model JOIN linkModelCategory ON (model.idModel = linkModelCategory.fkModelProject) JOIN category ON (category.idCategory = linkModelCategory.fkCategory) */
		// Convert query result to an array of domain objects
	
		$projects = array();
		//var_dump($projects);
		foreach ($result as $row) {
			$projectId = $row['idProject'];
			$projects[$projectId] = $this->buildDomainObject($row);
		}
		return $projects;
	}
	
	public function findProjectByUser($userId) {
		// The associated user is retrieved only once
		$user = $this->userDAO->find($userId);
	
		$sql = "SELECT project.* FROM user
				JOIN assignProjectUser ON (user.idUser = assignProjectUser.fkUser)
				JOIN project ON (project.idProject = assignProjectUser.fkProject)
				WHERE user.idUser=? order by projectStartDate DESC";
	
		$result = $this->getDb()->fetchAll($sql,array($userId));
	
		$projects = array();
		foreach ($result as $row) {
			$projectId = $row['idProject'];
			$project=$this->buildDomainObject($row);
			$project->setUser($user);
			$projects[$projectId] = $project;
		}
		return $projects;
	}
	
	
    /**
     * Return a list of all project, sorted by date (most recent first).
     *
     * @return array A list of all projects.
     */
/*	
    public function findAll() {
        $sql = "select * from project order by projectStartDate desc";
        $result = $this->getDb()->fetchAll($sql);
        // Convert query result to an array of domain objects
        $projects = array();
        foreach ($result as $row) {
            $projectId = $row['idProject'];
            $projects[$projectId] = $this->buildDomainObject($row);
        }
        return $projects;
    }
*/
    /**
     * Creates an Project object based on a DB row.
     *
     * @param array $row The DB row containing Project data.
     * @return \apiv1\Domain\Project
     */
    protected function buildDomainObject(array $row) {
        $project = new Project();
        $project->setId($row['idProject']);
        $project->setName($row['projectName']);
        $project->setClient($row['fkClient']);
        $project->setModel($row['projectModel']);
        $project->setStartDate($row['projectStartDate']);
        $project->setEndDate($row['projectEndDate']);
        $project->setState($row['projectState']);
        $project->setCreator($row['projectCreatorName']);
        return $project;
    }


    /**
     * Returns an project matching the supplied id.
     *
     * @param integer $id
     *
     * @return \apiv1\Domain\Project|throws an exception if no matching article is found
     */
    public function find($id) {
        $sql = "select * from project where idProject=?";
        $row = $this->getDb()->fetchAssoc($sql, array($id));

        if ($row)
            return $this->buildDomainObject($row);
        else
            throw new \Exception("No article matching id " . $id);
    }




    /**
     * Saves an project into the database.
     *
     * @param \apiv1\Domain\Project $project The project to save
     */
    public function save(Project $project) {
        $projectData = array(
            'projectName' => $project->getName(),
            'fkClient' => $project->getClient(),
        	'projectModel' => $project->getModel(),
        	'projectStartDate' => $project->getStartDate(),
        	'projectEndDate' => $project->getEndDate(),
        	'projectState' => $project->getState(),
        	'projectCreatorName' => $project->getCreator()
            );

        if ($project->getId()) {
            // The project has already been saved : update it
            $this->getDb()->update('project', $projectData, array('idProject' => $project->getId()));
        } else {
            // The project has never been saved : insert it
            $this->getDb()->insert('project', $projectData);
            // Get the id of the newly created project and set it on the entity.
            $id = $this->getDb()->lastInsertId();
            $project->setId($id);
        }
    }

    /**
     * Removes an project from the database.
     *
     * @param integer $id The project id.
     */
    public function delete($id) {
        // Delete the article
        $this->getDb()->delete('project', array('idProject' => $id));
    }

    // ...
}



