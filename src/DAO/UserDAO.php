<?php

namespace apiv1\DAO;

use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use apiv1\Domain\User;

class UserDAO extends DAO implements UserProviderInterface
{
	/**
	 * Return a list of all users, sorted by id .
	 *
	 * @return array A list of all users.
	 */
	
	public function findAll()
	{
		$sql = "select * from user order by idUser";
		$result = $this->getDb()->fetchAll($sql);
		//var_dump($result);
		// Convert query result to an array of domain objects
		$users = array();
		foreach ($result as $row) 
		{
			$userId = $row['idUser'];
			$users[$userId] = $this->buildDomainObject($row);
			//var_dump($users);
		}
		return $users;
	}	
	
    /**
     * Returns a user matching the supplied id.
     *
     * @param integer $id The user id.
     *
     * @return \apiv1\Domain\User|throws an exception if no matching user is found
     */
    public function find($id) {
        $sql = "select * from user where idUser=?";
        $row = $this->getDb()->fetchAssoc($sql, array($id));

        if ($row)
            return $this->buildDomainObject($row);
        else
            throw new \Exception("No user matching id " . $id);
    }
    
    
    /**
     * Removes an user from the database.
     *
     * @param integer $id The user id.
     */
    public function delete($id) {
    	// Delete the user
    	$this->getDb()->delete('user', array('idUser' => $id));
    }

    /**
     * {@inheritDoc}
     */
    
    /**
     * Saves an user into the database.
     *
     * @param \apiv1\Domain\User $user The user to save
     */
    public function save(User $user) {
    	$userData = array(
    			'userName' => $user->getUsername(),
    			'userLastname' => $user->getUserLastname(),
    			'userTrigramme' => $user->getUserTrigramme(),
    			'userLogin' => $user->getUserLogin(),
    			'userPassword' => $user->getPassword(),
    			'userCreationDate' => $user->getUserCreationDate(),
    			'userType' => $user->getUserType()
    	);
    
    	if ($user->getId()) {
    		// The user has already been saved : update it
    		$this->getDb()->update('user', $userData, array('idUser' => $user->getId()));
    	} else {
    		// The user has never been saved : insert it
    		$this->getDb()->insert('user', $userData);
    		// Get the id of the newly created user and set it on the entity.
    		$id = $this->getDb()->lastInsertId();
    		$user->setId($id);
    	}
    } 
    
    /**
     * {@inheritDoc}
     */
    public function loadUserByUsername($login)
    {
    	$sql = "select * from user where userLogin=?";
    	$row = $this->getDb()->fetchAssoc($sql, array($login));
    
    	if ($row)
    		return $this->buildDomainObject($row);
    		else
    			throw new UsernameNotFoundException(sprintf('User "%s" not found.', $login));
    }

    /**
     * {@inheritDoc}
     */
    public function refreshUser(UserInterface $user)
    {
        $class = get_class($user);
        if (!$this->supportsClass($class)) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', $class));
        }
        return $this->loadUserByUsername($user->getUsername());
    }

    /**
     * {@inheritDoc}
     */
    public function supportsClass($class)
    {
        return 'MicroCMS\Domain\User' === $class;
    }

    /**
     * Creates a User object based on a DB row.
     *
     * @param array $row The DB row containing User data.
     * @return \apiv1\Domain\User
     */
    protected function buildDomainObject(array $row) {
        $user = new User();
        $user->setId($row['idUser']);
        $user->setUsername($row['userName']);
        $user->setUserLastname($row['userLastname']);
        $user->setUserTrigramme($row['userTrigramme']);
        $user->setUserLogin($row['userLogin']);
        $user->setPassword($row['userPassword']);
        $user->setUserCreationDate($row['userCreationDate']);
        $user->setUserType($row['userType']);
     //   $user->setSalt($row['usr_salt']);
     //   $user->setRole($row['usr_role']);
        return $user;
    }
}