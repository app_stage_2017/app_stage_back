<?php 

namespace apiv1\DAO;

use apiv1\Domain\LinkModelCategory;


class LinkModelCategoryDAO extends DAO 
{
	/**
	 * Creates an linkModelCategory object based on a DB row.
	 *
	 * @param array $row The DB row containing linkModelCategory data.
	 * @return \apiv1\Domain\LinkModelCategory
	 */
	protected function buildDomainObject(array $row) {
		$modelCategory = new LinkModelCategoryDAO();
		$modelCategory->setIdModel($row['fkModel']);
		$modelCategory->setidCategory($row['fkCategory']);
		return $modelCategory;
	}
	
	/**
	 * Saves an linkModelCategory into the database.
	 *
	 * @param \apiv1\Domain\LinkModelCategory $linkModelCategory The linkModelCategory to save
	 */
	public function save(LinkModelCategory $linkModelCategory) {
		$linkModelCategoryData = array(
				'fkModel' => $linkModelCategory->getIdModel(),
				'fkCategory' => $linkModelCategory->getidCategory()
	
		);
		if ($linkModelCategory->getIdLinkModelCategory()) {
			// The linkModelCategory has already been saved : update it
			$this->getDb()->update('linkModelCategory', $linkModelCategoryData, array('idLinkModelCategorie' => $linkModelCategory->getIdLinkModelCategory()));
		} else {
			// The The linkModelCategory has never been saved : insert it
			$this->getDb()->insert('linkModelCategory', $linkModelCategoryData);
			// Get the id of the newly created project and set it on the entity.
			$id = $this->getDb()->lastInsertId();
			$linkModelCategory->setLinkModelCategory($id);
		}
	}
	
	/**
	 * Removes an linkModelCategory from the database.
	 *
	 * @param integer $id The idlinkModelCategory id.
	 */
	public function delete($id) {
		// Delete the article
		$this->getDb()->delete('linkModelCategory', array('idLinkModelCategorie' => $id));
	}

}