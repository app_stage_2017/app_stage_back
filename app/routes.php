<?php 

//----------------------------------------------------------------------------------------//
//----------------------------------------- ROUTES REMOVE---------------------------------//
//----------------------------------------------------------------------------------------//
// API : remove an project
$app->delete('/api/projects/{id}', "apiv1\Controller\ApiProjectsController::deleteProject");

//API: remove an taskproject
$app->delete('/api/tasks/{id}', "apiv1\Controller\ApiTasksProjectsController::deleteTask");

//API : remove an model
$app->delete('/api/models/{id}', "apiv1\Controller\ApiModelsCategoryTaskController::deleteModel");

//API : remove an category
$app->delete('/api/categories/{id}', "apiv1\Controller\ApiModelsCategoryTaskController::deleteCategory");

//API : remove an user
$app->delete('/api/users/{id}', "apiv1\Controller\ApiUsersController::deleteUser");

//API: cremove client
$app->delete('/api/clients/{id}',"apiv1\Controller\ApiClientController::deleteClient");

//-----------------------------------------------------------------’-----------------------//
//----------------------------------------- ROUTES UPDATE---------------------------------//
//----------------------------------------------------------------------------------------//


//API : update an project patch method
$app->patch('/api/projects/{id}',"apiv1\Controller\ApiProjectsController::updateProjectById");

//API : update an taskproject patch method
$app->patch('/api/tasks/{id}',"apiv1\Controller\ApiTasksProjectsController::updateTaskById");

//----------------------------------------------------------------------------------------//
//----------------------------------------- ROUTES GET BY ID------------------------------//
//----------------------------------------------------------------------------------------//

//API : get category by idModel
$app->get('/api/categories/{id}', "apiv1\Controller\ApiModelsCategoryTaskController::getCategoryByIdModel");

//API : get projects by idUser
$app->get('/api/projects/{id}', "apiv1\Controller\ApiProjectsController::getProjectByIdUser");

//API : get logs by idTask
$app->get('/api/logs/{id}', "apiv1\Controller\ApiLogController::getLogByIdTask");


/*
//API : get task by id 
$app->get('/api/tasks/{id}',"apiv1\Controller\ApiTasksProjectsController::getTaskById");
*/

//----------------------------------------------------------------------------------------//
//----------------------------------------- ROUTES CREATE------------------------------//
//----------------------------------------------------------------------------------------//


// API : create an project
$app->post('/api/projects', "apiv1\Controller\ApiProjectsController::createProject");

//API : create an model
$app->post('/api/models', "apiv1\Controller\ApiModelsCategoryTaskController::createModel");

//API : create an category
$app->post('/api/categories', "apiv1\Controller\ApiModelsCategoryTaskController::createCategory");

//API : create a logs by idTask
$app->post('/api/logs', "apiv1\Controller\ApiLogController::createLog");

//API : login
$app->post('/api/login',"apiv1\Controller\ApiLoginController::authenticate");

//API : create taskProject
$app->post('/api/tasks',"apiv1\Controller\ApiTasksProjectsController::createTaskProject");

//API : create assignProjectUser
$app->post('/api/assign',"apiv1\Controller\ApiAssignProjectUserController::assignProjetUser");

//API : create LinkModelCategory
$app->post('/api/assigncategorytomodel',"apiv1\Controller\ApiModelsCategoryTaskController::linkModelCategory");

//API : create taskModel
$app->post('/api/taskmodel',"apiv1\Controller\ApiModelsCategoryTaskController::createTaskModel");

//API : create user
$app->post('/api/users',"apiv1\Controller\ApiUsersController::createUser");

//API: create file
$app->post('/api/files',"apiv1\Controller\ApiFileController::createFile");

//API: create client
$app->post('/api/clients',"apiv1\Controller\ApiClientController::createClient");
//----------------------------------------------------------------------------------------//
//----------------------------------------- ROUTES GET------------------------------//
//----------------------------------------------------------------------------------------//



/*
// API : get all tasks
$app->get('/api/tasks', "apiv1\Controller\ApiTasksProjectsController::getTasks");
*/

// API : get all projects
$app->get('/api/projects', "apiv1\Controller\ApiProjectsController::getProjects");

//API : get all models with category a
$app->get('/api/models',"apiv1\Controller\ApiModelsCategoryTaskController::getModels");

//API : get all user
$app->get('/api/users',"apiv1\Controller\ApiUsersController::getUsers");

//API : get all client
$app->get('/api/clients',"apiv1\Controller\ApiClientController::getClients");



//API : get all categories
$app->get('/api/categories',"apiv1\Controller\ApiModelsCategoryTaskController::getCategories");





