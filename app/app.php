<?php

use Symfony\Component\Debug\ErrorHandler;
use Symfony\Component\Debug\ExceptionHandler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use apiv1\DAO\AttachedFileDAO;



// Register global error and exception handlers
ErrorHandler::register();
ExceptionHandler::register();


// Register service providers.
$app->register(new Silex\Provider\DoctrineServiceProvider());


//create monolog
$app->register(new Silex\Provider\MonologServiceProvider(), array(
		'monolog.logfile' => __DIR__.'/../var/logs/microcms.log',
		'monolog.name' => 'MicroCMS',
		'monolog.level' => $app['monolog.level']
));

//create an uploaddirectory
$app['upload.directory'] = function ($app)
{
	//return dirname(__DIR__).'\var\uploads'; // POUR WIN
	return dirname(__DIR__).'uploads'; //POUR MAC
};

// securite et connexion

// Register services
//create Project DAO
$app['dao.project'] = function ($app) {
	$projectDAO = new apiv1\DAO\ProjectDAO($app['db']);
	$projectDAO->setUserDAO($app['dao.user']);
	return $projectDAO;
	
};


//create TaskProject DAO
$app['dao.taskproject'] = function ($app) {
    $taskProjectDAO = new apiv1\DAO\TaskProjectDAO($app['db']);
    $taskProjectDAO->setProjectDAO($app['dao.project']);
    return $taskProjectDAO;
};


//create Model DAO
$app['dao.model'] = function ($app) {
	return new apiv1\DAO\ModelDAO($app['db']);
};

//create Category DAO
$app['dao.category'] = function ($app) {
	$categoryDAO = new apiv1\DAO\CategoryDAO($app['db']);
	$categoryDAO->setModelDAO($app['dao.model']);
	return $categoryDAO;	
};

//create Task DAO
$app['dao.task'] = function ($app) {
	$taskDAO = new apiv1\DAO\TaskDAO($app['db']);
	$taskDAO->setCategoryDAO($app['dao.category']);
	return $taskDAO;
};

//create LogTask DAO
$app['dao.logtaskhistory'] = function ($app) {
	$logtaskhistoryDAO = new apiv1\DAO\LogTaskHistoryDAO($app['db']);
	return $logtaskhistoryDAO;
};

//create User DAO
$app['dao.user'] = function ($app) {
	return new apiv1\DAO\UserDAO($app['db']);
};

//create AssignUserproject DAO
$app['dao.assignprojectuser'] = function ($app) {
	return new apiv1\DAO\AssignProjectUserDAO($app['db']);
};

//create Client DAO
$app['dao.client'] = function ($app) {
	return new apiv1\DAO\ClientDAO($app['db']);
};



//create LinkmodelCategory DAO
$app['dao.linkmodelcategory'] = function ($app) {
	return new apiv1\DAO\LinkModelCategoryDAO($app['db']);
};

//create AttachedFileDAO
$app['dao.attachedFile'] = function ($app)
{
	return new apiv1\DAO\AttachedFileDAO($app['db']);
};


//create ModelAttachedFileDAO
$app['dao.modelattachedfile'] = function ($app)
{
	return new apiv1\DAO\ModelAttachedFileDAO($app['db']);
};




//middleware
//cors
$app->after(function (Request $request, Response $response) {
	$response->headers->set('Access-Control-Allow-Origin', '*');
	$response->headers->set('Access-Control-Allow-Headers',
			'Origin, X-Requested-With, Content-Type, Accept, Authorization');
	$response->headers->set('Access-Control-Allow-Methods',' GET, POST, PUT,PATCH,DELETE');
});

$app->options("{anything}", function () {
		return new \Symfony\Component\HttpFoundation\JsonResponse(null, 204);
})->assert("anything", ".*");


// Register JSON data decoder for JSON requests
$app->before(function (Request $request) {
    if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
        $data = json_decode($request->getContent(), true);
        $request->request->replace(is_array($data) ? $data : array());
    }
  
    
});
