# Install Composer
https://getcomposer.org/

# Install Project 
* Clone your project on your local.
* On the projet directory `composer update`.

# Install DATABASE
* Create an database `apirecette` on Mysql
* Insert script `db/structure.sql`
* Insert scrit `db/data.slq`
