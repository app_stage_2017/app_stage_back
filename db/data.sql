/*appel à la base*/
USE apirecette;

/*Models*/
    /*table Models*/
    INSERT INTO `model` (`idModel`,`modelName`) 
    VALUES
    ("1","Editique"),
    ("2","LCH"),
    ("3","LCR"),
    ("4","BOR"),
    ("5","Titres"),
    ("6","Annexes"),
    ("7","Web");

    /*table Category*/
    INSERT INTO `category` (`idCategory`,`categoryName`) 
    VALUES
    ("1","Transmission"),
    ("2","Production Informatique"),
    ("3","Matières Premières"),
    ("4","Chargé de Compte"),
    ("5","Parties Communes"),
    ("6","Recette Client"),
    ("7","Editique"),
    ("8","Lettre Cheque"),
    ("9","Partie Cheque"),
    ("10","LCR"),
    ("11","Titres"),
    ("12","Gestion des Annexes"),
    ("13","Gestion des Recommandés"),
    ("14","Reporting Web"),
    ("15","Livraison en production"),
    ("16","Contrôle 1ere production"),
    ("17","Documentation"),
    ("18","Integration du document finalisé"),
    ("19","Specifique Développement"),
    ("20","Specifique Bureau d'études");
 /*   ("21","Specifique"); */

    /*table Task*/
    INSERT INTO `task` (`idTask`,`taskName`,`fk_category`,`taskDescription`,`categoryTask`,`userType`) 
    VALUES
    ("1","Flux reception réalisé","1","","Transmission",""),
    ("2","Flux retour réalisé","1","","Transmission",""),
    ("3","Purge fichier télétransmission","2","","Production Informatique",""),
    ("4","Commande Support Impression","3","","Matières Premières",""),
    ("5","Commande Envelope","3","","Matières Premières",""),
    ("6","Annexes","3","","Matières Premières",""),
    ("7","Cahier Clauses Techniques Particulières","4","","Chargé de Compte",""),
    ("8","Création Code Client","4","","Chargé de Compte",""),
    ("9","Création Fiche Redmine + SLA","4","","Chargé de Compte",""),
    ("10","Validation Mise en Page","4","","Chargé de Compte",""),
    ("11","Validation présence des données validées par le client","4","","Chargé de Compte",""),
    ("12","Validation Facturation","4","","Chargé de Compte",""),
    ("13","Validation par retour du BAT validé du Client","4","","Chargé de Compte",""),
    ("14","Validation par retour du BAT validé par la Banque","4","","Chargé de Compte",""),
    ("15","Fournir code client au TIAC","4","Fournir le code client au TIAC pour paramétrage Base Affranchissement METHERNET","Chargé de Compte",""),
    ("16","Kickoff interne","5","Kickoff interne (Production/Bureau d'étude/CDC)","Parties Communes",""),
    ("17","Dossier de cadrage validé par le client","5","","Parties Communes",""),
    ("18","Paramétrage RCP Param","5","","Parties Communes",""),
    ("19","Fichier test fourni","5","Fichier test fourni reprenant les cas de figure prévus dans le dossier de cadrage client","Parties Communes",""),
    ("20","Dossier de MEP HAPPII ou dossier de Spécifications livré à GMDSIPROD","5","","Parties Communes",""),
    ("21","Respect de la Mise en Page du document suivant modèle client","5","","Parties Communes",""),
    ("22","Respect de la charte graphique","5","","Parties Communes",""),
    ("23","Respect du Mode d’impression : Recto ou Recto/Verso","5","","Parties Communes",""),
    ("24","Pavé adresse Standard","5","Rappel de la Norme Postale : Position du bloc adresse conforme et compatible au standard C4/C5/C6. Alignement strict des lignes à gauche. Police de caractères conforme aux normes Postale : Lucinda Console 10 (police préconisée).Composition en « CLOSEUP » empilage des lignes du bas vers le haut .Position du code postal fixe.Code Postal sur 5 caractères pour la France.38 caractères maxi par lignes maximum.Casse majuscule obligatoire.Pas de pays pour la France mais uniquement pour les étrangers.Jeu de caractères limité et standard - Pas de caractères accentués, ni de ponctuation.Pas de ligne blanche dans le pavé adresse.Respect de la zone de silence (position xy ; largeur ; hauteur).Ordre des lignes respecté","Parties Communes",""),
    ("25","Gestion des tranches de poids","5","","Parties Communes",""),
    ("26","Gestion des envois à destination de l’étranger","5","","Parties Communes",""),
    ("27","Gestion de la taille des lots/limite de spool","5","","Parties Communes",""),
    ("28","Reporting Web ","5","Déclenchement des éditions par validation (massification…)","Parties Communes",""),
    ("29","Lancement VEGA","5","","Parties Communes",""),
    ("30","Présence de la ligne technique sur la 1ere feuille et feuille suivante","5","","Parties Communes",""),
    ("31","Code client lisible pour identification PND","5","","Parties Communes",""),
    ("32","Numéro de pli apprent en clair","5","","Parties Communes",""),
    ("33","Code à barre de type : 2 parmi 5","5","","Parties Communes",""),
    ("34","Présence des Codes OMR","5","1er trait de position (contrôle lecture) à 12 cm du bord haut de la feuille sur toutes les feuilles et à 2mm du bord droit de la feuille et à l’identique sur les vues gauches et droites et les pages suite ","Parties Communes",""),
    ("35","Code Barres de contrôle","5","Intégralité de type 2 parmi 5 côté droit : var1 : 7 (ht du code barre) var2 : 11 (taille de la police utilisée)","Parties Communes",""),
    ("36","Présence du Datamatrix","5","sur la première feuille : Ht/Largeur identique 7mm","Parties Communes",""),
    ("37","Présence du Numéro de lot","5","sur 12 positions track ID","Parties Communes",""),
    ("38","Numéros visibles à travers la fenêtre de l enveloppe","5","Numéro de lot, Numéro de page, Numéro de pli et code client (CLXXXX) qui doivent être visibles à travers la fenêtre de l’enveloppe","Parties Communes",""),
    ("39","Respect contrôle affranchissement","5","Courrier industriel /hors CI/Type d’affranchissement","Parties Communes",""),
    ("40","Concordance","5","edition en continue document maître/esclave","Parties Communes",""),
    ("41","Vérification cohérence Recto/Verso","5","","Parties Communes",""),
    ("42","Mécanisable/non mécanisable","5","","Parties Communes",""),
    ("43","Recette de l'OF","5","","Parties Communes",""),
    ("44","Contrôle du 1er l'OF de production ","5","numéro de lot présent","Parties Communes",""),
    ("45","Contrôle de l'OFAB","5","","Parties Communes",""),
    ("46","Mise en place Fichiers Retours","5","","Parties Communes",""),
    ("47","Mise en place Mail de Validation","5","","Parties Communes",""),
    ("48","Vérification fichier controle","5","","Parties Communes",""),
    ("49","Synthèse production livrée aux TIAC","5","","Parties Communes",""),
    ("50","Production à blanc","5","","Parties Communes",""),
    ("51","BAT","5","Mention « spécimen SATI ou Annulé » lors de l’impression des BAT. Envoi au client.Envoi à la banque.Retour client","Parties Communes",""),
    ("52","HAPPII : purge des fichiers dans le frigo","5","","Parties Communes",""),
    ("53","Vérification 1er fichier de test client","6","","Recette Client",""),
    ("54","Vérification lors de la première production si TMA","6","","Recette Client",""),
    ("55","Contrôle mise en page par rapport au modèle fourni par le client","7","","Editique",""),
    ("56","Contrôle mise en page par rapport au modèle client","8","","Lettre Cheque",""),
    ("57","Mise en place d'un fichier retour à la banque","8","","Lettre Cheque",""),
    ("58","Présence logo Banque","9","","Partie Cheque",""),
    ("59","Présence mention FHD, si Optoseal (Niort)","9","","Partie Cheque",""),
    ("60","Présence code saphir, si Optoseal (Niort)","9","","Partie Cheque",""),
    ("61","Fonte OCBR","9","absence code OM que pour les codes Saphir","Partie Cheque",""),
    ("62","Référence support pré-imprimé dans le talon","9","","Partie Cheque",""),
    ("63","Dépose d'hologramme","9","systématique avec le FHD","Partie Cheque",""),
    ("64","Verification Zone bénéficiaire","9","espace rempli avec ****","Partie Cheque",""),
    ("65","Vérification Zone somme en toutes lettres ","9","espace rempli avec ****","Partie Cheque",""),
    ("66","Vérification que le Montant en chiffres ","9","précédés et suivis par ****","Partie Cheque",""),
    ("67","Verification du sigle €","9","positionnement et taille (en contact avec le cadre du montant en chiffres)","Partie Cheque",""),
    ("68","Vérification de la présence de l’optoseal à rédiger exclusivement en euros ","9","au dessus du cadre montant en chiffres","Partie Cheque",""),
    ("69","Vérification du nombre de caractères de la zone bénéficiaire ","9","Celui-ci doit être situé entre 47 et 50 caractères, les * comprises (taille en fonction de la police utilisée)","Partie Cheque",""),
    ("70","Vérification la présence de la mention case payable en France sur le pré imprimé ","9","si non, penser à l’intégrer ","Partie Cheque",""),
    ("71","Présence du numéro de chèque en clair","9","","Partie Cheque",""),
    ("72","Vérification des informations du compte tireur ou client ","9","ainsi que la désignation : nom client/adresse soient présentes","Partie Cheque",""),
    ("73","Vérification que le Zone Lieu (à….) soit présente","9","","Partie Cheque",""),
    ("74","Vérification que la Date (le) soit présente","9","","Partie Cheque",""),
    ("75","Vérification de la présence et la validité de la première signature ","9","","Partie Cheque",""),
    ("76","Vérification des conditions de présence et la validité de la seconde signature ","9","plafond de mise en place","Partie Cheque",""),
    ("77","Verification clé RLMC ou clé Modulo","9","Vérification de la présence de la Clé de Recomposition de la Ligne Magnétique du Chèque dite « clé RLMC » ou Clé Modulo : Vérification du calcul – position - entouré de <et>","Partie Cheque",""),
    ("78","Présence de la ligne CMC7","9","- Ligne CMC7 : respect de la zone de silence : 16 mm du bas de la page sur la longueur du chèque (175 mm) à partir du bord droit de la feuille.Ligne CMC7 : 35 caractères en fonte CMC7 (CMC74P.ipf) 1 car : 'O' - 7 car : numéro de chèque - 1 car : 'O' - 12 car : n° de compte interbancaire - 1 car : 'X' - 12 car : compte client CMC7 - 1 car : 'T'. Suivant les cas, il peut y avoir un ou plusieurs espaces, voir la définition CMC7 client.Ligne CMC7 : commence à 5 mm à droite du talon du chèque, se termine à au moins 51 mm du bord droit de la feuille","Partie Cheque",""),
    ("79","Ligne technique sur les pages  suite ","9","à intégrer au milieu côté gauche et non sur la droite du document","Partie Cheque",""),
    ("80","Contrôle mise en page par rapport au modèle client","10","","LCR",""),
    ("81","Ligne technique sur les pages suite est à intégrer au milieu du côté gauche et NON sur la droite du document","10","","LCR",""),
    ("82","Respect de la définition de la ligne CMC7/de l’OCRB/du code à barre client /Validation","11","","Titres",""),
    ("83","Poids du pli intégré dans le fichier de contrôle","11","","Titres",""),
    ("84","Type de job paramétré dans le fichier de contrôle","11","","Titres",""),
    ("85","Code affranchissement paramétré dans le fichier de contrôle","11","","Titres",""),
    ("86","Code impact Böwe (pavés noirs /codes « W »)","11","","Titres",""),
    ("87","Gestion des Étiquettes","12","","Gestion des Annexes",""),
    ("88","Gestion des Bordereaux","12","","Gestion des Annexes",""),
    ("89","Gestion des Bons de Commande","12","","Gestion des Annexes",""),
    ("90","Gestion des Bons de Livraison","12","","Gestion des Annexes",""),
    ("91","Typologie des annexes ","12","avec codes-barres/sans code barre","Gestion des Annexes",""),
    ("92","Mode de mise à jour","12","","Gestion des Annexes",""),
    ("93","Rythme des mises à jour","12","","Gestion des Annexes",""),
    ("94","Annexes de mêmes références attribuées aux mêmes margeurs","12","","Gestion des Annexes",""),
    ("95","Reco : Alimentation de la Liasse LIRE","13","","Gestion des Recommandés",""),
    ("96","Reco : Alimentation du Bordereau ","13","","Gestion des Recommandés",""),
    ("97","Encodage en UTF8","14","","Reporting Web",""),
    ("98","Pré requis reporting Web","14","","Reporting Web",""),
    ("99","Logo personnalisé","14","","Reporting Web",""),
    ("100","Destinataire mail d’information du JOB, autre que Superviseur et Client","14","","Reporting Web",""),
    ("101","Massification Active","14","","Reporting Web",""),
    ("102","Vérification que l’ensemble du projet soit bien basculé dans les répertoires de production ","15","","Livraison en production",""),
    ("103","Mise en production du reporting WEB","15","","Livraison en production",""),
    ("104","Accessibilité pour les nouveaux usagers client","15","","Livraison en production",""),
    ("105","Vérification 1ère production","16","","Contrôle 1ere production",""),
    ("106","Document de description fonctionnelle","17","","Documentation",""),
    ("107","Commentaire sur les fichiers sources","17","","Documentation",""),
    ("108","Procédures à suivre pour les interventions récurrentes (mise à jour d’insertions,…)","17","","Documentation",""),
    ("109","Scan document dans répertoire : ","18","","Integration du document finalisé",""),
    ("110","Paramétrages Happii effectué ","19","facturation, PRM, Actarus","Specifique Développement",""),
    ("111","Si numerisation AR, PND ","19","Paramétrer la sortie des fiches de lot HAPPII","Specifique Développement",""),
    ("112","Coffre ","19","Nex obligatoire","Specifique Développement",""),
    ("113","Réception de la Synthèse de production dûment renseignée et livrée (Développeur) ","20","","Specifique Bureau d'études",""),
    ("114","Contrôle du Code client ","20","","Specifique Bureau d'études",""),
    ("115","Vérification de la composition du Pavé adresse","20","(police, zone de silence, composé en close up, pas de ligne blanche) du destinataire et de son cadrage en vu de la MSP","Specifique Bureau d'études",""),
    ("116","Paramétrages Methernet effectué :","20","A réception code Client de la part des CDC","Specifique Bureau d'études",""),
    ("117","Contrôle des Codes GPAO ","20","","Specifique Bureau d'études",""),
    ("118","Contrôle de la date butoir sur OFAB HAPPII","20","","Specifique Bureau d'études",""),
    ("119","Mise en place de la Fiche POPR ","20","","Specifique Bureau d'études",""),
    ("120","Mise en place de la Fiche de fab Edition  ","20","","Specifique Bureau d'études",""),
    ("121","Mise en place de la Fiche de fab Façonnage ","20","","Specifique Bureau d'études",""),
    ("122","Mise en place de la Fiche de fab Colisage ","20","","Specifique Bureau d'études",""),
    ("123","Mise en place de la Fiche de fab Mise sous Pli  ","20","","Specifique Bureau d'études",""),
    ("124","Mise en place de la Fiche de fab traitement des AR :","20","Numérisation, Flashage, Renvoi","Specifique Bureau d'études",""),
    ("125","Mise en place de la Fiche de fab Bordereaux de dépôt:","20","Numérisation, Flashage, Renvoi, Destruction","Specifique Bureau d'études",""),
    ("126","Sens du fichier de contrôle conforme à l’édition","20","","Specifique Bureau d'études",""),
    ("127","Vérification de la présence de la flamme sur l’enveloppe","20","","Specifique Bureau d'études",""),
    ("128","Vérification que le poids de la k7 mini 5 kg pour les CP et D","20","","Specifique Bureau d'études",""),
    ("129","Vérification que le bordereau de dépôt poste soit conforme aux normes postales","20","","Specifique Bureau d'études",""),
    ("130","TIP adapté aux nouvelles normes SEPA ","20","-	Voir documentation dans les deux pièces jointes","Specifique Bureau d'études",""),
    ("131","Vérification de la présence de la Clé Modulo ou RLMC ","20","","Specifique Bureau d'études",""),
    ("132","Vérification de la présence de la ligne CMC7 et de la validité dans le qualifieur","20","","Specifique Bureau d'études",""),
    ("133","Vérification des codes de gestion des plis ","20","(lignes techniques/code Intégralité/code OMR)","Specifique Bureau d'études",""),
    ("134","Vérification de la conformité du cadrage de la liasse lire après édition","20","","Specifique Bureau d'études",""),
    ("135","Intégration référentiel Client/La Poste ","20","PLI,Bordereau,Etiquette","Specifique Bureau d'études",""),
    ("136","Soumission de la Validation à La Poste","20","","Specifique Bureau d'études",""),
    ("137","Vérification de la 1ère production ","20","","Specifique Bureau d'études",""),
    ("138","Position du bloc adresse conforme et compatible au standard C4/C5/C6","5","","Parties Communes",""),
    ("139", "Alignement stict des lignes à gauche","5","","Parties Communes",""),
    ("140", "Police de caractères conforme aux normes Postale: Lucinda Console 10 (police préconisée)","5","","Parties Communes",""),
    ("141", "Composition en CLOSEUP empilage des lignes du bas vers le haut","5","","Parties Communes",""),
    ("142", "Position du code postal fixe","5","","Parties Communes",""),
    ("143", "Code Postal sur 5 caractères pour la France","5","","Parties Communes",""),
    ("144", "38 caractères maxi par lignes maximum","5","","Parties Communes",""),
    ("145", "Casse majuscule obligatoire","5","","Parties Communes",""),
    ("146", "Pas de pays pour la France mais uniquement pour les étrangers","5","","Parties Communes",""),
    ("147", "Jeu de caractères limité et standard.Pas de caractères accentués, ni de ponctuation","5","","Parties Communes",""),
    ("148", "Pas de ligne blanche dans le pavé adresse","5","","Parties Communes",""),
    ("149", "Respect de la zone de silence (position xy ; largeur ; hauteur)","5","","Parties Communes",""),
    ("150", "Ordre des lignes respecté","5","","Parties Communes","");


    /*table de liaison linkModelCategory */
    INSERT INTO `linkModelCategory` (`idLinkModelCategorie`,`fkModel`,`fkCategory`) 
    VALUES
    ("1","1","1"),
    ("2","1","2"),
    ("3","1","3"),
    ("4","1","4"),
    ("5","1","5"),
    ("6","1","6"),
    ("7","1","7"),
    ("8","1","13"),
    ("9","1","15"),
    ("10","1","16"),
    ("11","1","17"),
    ("12","1","18"),
    ("13","1","19"),
    ("14","1","20"),
 /*   ("1","21"), */
    ("15","2","1"),
    ("16","2","2"),
    ("17","2","3"),
    ("18","2","4"),
    ("19","2","5"),
    ("20","2","6"),
    ("21","2","8"),
    ("22","2","9"),
    ("23","2","13"),
    ("24","2","15"),
    ("25","2","16"),
    ("26","2","17"),
    ("27","2","18"),
    ("28","2","19"),
    ("29","2","20"),
 /*   ("2","21"), */
    ("30","3","1"),
    ("31","3","2"),
    ("32","3","3"),
    ("33","3","4"),
    ("34","3","5"),
    ("35","3","6"),
    ("36","3","10"),
    ("37","3","13"),
    ("38","3","15"),
    ("39","3","16"),
    ("40","3","17"),
    ("41","3","18"),
    ("42","3","19"),
    ("43","3","20"),
 /*   ("3","21"), */
    ("44","4","1"),
    ("45","4","2"),
    ("46","4","3"),
    ("47","4","4"),
    ("48","4","5"),
    ("49","4","6"),
    ("50","4","13"),
    ("51","4","15"),
    ("52","4","16"),
    ("53","4","17"),
    ("54","4","18"),
    ("55","4","19"),
    ("56","4","20"),
 /*   ("4","21"), */
    ("57","5","1"),
    ("58","5","2"),
    ("59","5","3"),
    ("60","5","4"),
    ("61","5","5"),
    ("62","5","6"),
    ("63","5","11"),
    ("64","5","13"),
    ("65","5","15"),
    ("66","5","16"),
    ("67","5","17"),
    ("68","5","18"),
    ("69","5","19"),
    ("70","5","20"),
 /*   ("5","21"), */
    ("71","6","1"),
    ("72","6","2"),
    ("73","6","3"),
    ("74","6","4"),
    ("75","6","5"),
    ("76","6","6"),
    ("77","6","12"),
    ("78","6","15"),
    ("79","6","16"),
    ("80","6","17"),
    ("81","6","18"),
    ("82","6","19"),
    ("83","6","20"),
 /*   ("6","21"), */
    ("84","7","1"),
    ("85","7","2"),
    ("86","7","3"),
    ("87","7","4"),
    ("88","7","5"),
    ("89","7","6"),
    ("90","7","14"),
    ("91","7","15"),
    ("92","7","16"),
    ("93","7","17"),
    ("94","7","18"),
    ("95","7","19"),
    ("96","7","20");
/*    ("7","21"); */

    /*User*/

        /*table User*/
     INSERT INTO `user` (`idUser`,`userName` ,`userLastname`,`userTrigramme`,`userLogin`,`userPassword`,`userCreationDate`,`userType`)
    VALUE
     ("1","latapie","romain","rla","romaincefim@gmail.com","romain123","2016/02/17","Dev"),
     ("2","toto","toto","tot","toto@gmail.com","toto123","2016/02/17","TIAC"),
     ("3","titi","titi","tit","titi@gmail.com","titi123","2016/02/17","Compte"),
     ("4","admin","admin","adm","adm@gmail.com","adm123","2016/02/17","Admin"); 

     /*Projects*/
        /*table Client*/
    INSERT INTO `client` (`idClient`,`clientName`,`clientCode`) 
    VALUE
     ("1","client1","code1"),
     ("2","client2","code2"),
     ("3","client3","code3"),
     ("4","client4","code4");

    /*TABLE PROJECT*/
    INSERT INTO `project` (`idProject`,`projectName`,`fkClient`,`projectModel`,`projectStartDate`,`projectEndDate`,`projectState`,`projectCreatorName`)
    VALUE
    ("1","projet1","1","editique","","","actif","user1"),
    ("2","projet2","1","LCR","","","actif","user2");

    /*TABLE ASSIGN PROJECT USER*/
    INSERT INTO `assignProjectUser` (`idProjectUser`,`fkProject`,`fkUser`)
    VALUE
    ("1","1","1"),
    ("2","1","2"),
    ("3","2","2");

    /*TABLE TASKPorject*/
    INSERT INTO `taskProject` (`idTaskProject`,`taskProjectName`,`taskProjectDescirption`,`taskProjectStatut`,`taskProjectCommentary`,`taskProjectCategoryName`,`taskCreationDate`,`taskEndDate`,`fkproject`,`taskUserType`)
    VALUE
    ("1","Flux reception réalisé","","sans","","Transmission","","","1","Dev"),
    ("2","Flux retour réalisé","","sans","","Transmission","","","1","Dev"),
    ("3","Purge fichier télétransmission","","sans","","Production Informatique","","","1","Dev"),
    ("4","Commande Support Impression","","sans","","Matières Premières","","","1","Dev"),
    ("5","Commande Envelope","","sans","","Matières Premières","","","1","Dev"),
    ("6","Annexes","","sans","","Matières Premières","","","1","Dev"),
    ("7","Cahier Clauses Techniques Particulières","","sans","","Chargé de Compte","","","1","Dev"),
    ("8","Création Code Client","","sans","","Chargé de Compte","","","1","Dev"),
    ("9","Création Fiche Redmine + SLA","","sans","","Chargé de Compte","","","1","Dev"),
    ("10","Validation Mise en Page","","sans","","Chargé de Compte","","","1","Dev"),
    ("11","Validation présence des données validées par le client","","sans","","Chargé de Compte","","","1","Dev"),
    ("12","Validation Facturation","","sans","","Chargé de Compte","","","1","Dev"),
    ("13","Validation par retour du BAT validé du Client","","sans","","Chargé de Compte","","","1","Dev"),
    ("14","Validation par retour du BAT validé par la Banque","","sans","","Chargé de Compte","","","1","Dev"),
    ("15","Fournir code client au TIAC","Fournir le code client au TIAC pour paramétrage Base Affranchissement METHERNET","sans","","Chargé de Compte","","","1","Dev"),
    ("16","Flux reception réalisé","","sans","","Transmission","","","2","Dev"),
    ("17","Flux retour réalisé","","sans","","Transmission","","","2","Dev"),
    ("18","Purge fichier télétransmission","","sans","","Production Informatique","","","2","Dev"),
    ("19","Commande Support Impression","","sans","","Matières Premières","","","2","Dev"),
    ("20","Commande Envelope","","sans","","Matières Premières","","","2","Dev"),
    ("21","Annexes","","sans","","Matières Premières","","","2","Dev"),
    ("22","Cahier Clauses Techniques Particulières","","sans","","Chargé de Compte","","","2","Dev"),
    ("23","Création Code Client","","sans","","Chargé de Compte","","","2","Dev"),
    ("24","Création Fiche Redmine + SLA","","sans","","Chargé de Compte","","","2","Dev"),
    ("25","Validation Mise en Page","","sans","","Chargé de Compte","","","2","Dev"),
    ("26","Validation présence des données validées par le client","","sans","","Chargé de Compte","","","2","Dev"),
    ("27","Validation Facturation","","sans","","Chargé de Compte","","","2","Dev"),
    ("28","Validation par retour du BAT validé du Client","","sans","","Chargé de Compte","","","2","Dev"),
    ("29","Validation par retour du BAT validé par la Banque","","sans","","Chargé de Compte","","","2","Dev"),
    ("30","Fournir code client au TIAC","Fournir le code client au TIAC pour paramétrage Base Affranchissement METHERNET","sans","","Chargé de Compte","","","2","Dev");

    /*TABLE logTaskHistory*/
    INSERT INTO `logTaskHistory` (`idLogTaskHistory`,`fkTask`,`dateLogTask`,`userTrigramme`,`typeLog`,`commentary`)
    VALUE
    ("1","1","2017-03-15 09:09:35","rla","ok",""),
    ("2","1","2017-03-29 14:29:40","tot","ko",""),
    ("3","10","2017-03-30 14:28:37","rla","commentaire",""),
    ("4","10","2017-03-20 10:17:42","tot","fichier","");

    /*TABLE attachedFile*/
     INSERT INTO `attachedFile`(`idAttachedFile`,`attachedFileName`,`attachedFilePath`,`attachedFileIdTak`)
    VALUE
    ("1","essaipdf","essai.pdf","1");

    /*TABLE modelattachedFile*/
     INSERT INTO `modelattachedFile`(`idModelAttachedFile`,`modelAttachedFileName`,`modelAttachedFilePath`,`modelAttachedFileIdTak`)
    VALUE
    ("1","teletransmissions","\\chsfi005\Services\DSI\SALOME\TRANSMISSION OK\Transmissions ALLER RETOUR.pdf","1"),
    ("2","teletransmissions","\\chsfi005\Services\DSI\SALOME\TRANSMISSION OK\Transmissions ALLER RETOUR.pdf","2"),
    ("3","MEP HAPPII","\\chsfi005.multipap.corp\DOCS\Docs-DSI\Exploit\Mod_DPSI_5_Dossier_de_spécifications_techniques_CLIENT-PROJET.pdf","20"),
    ("4","pave adresse","\\chsfi005.multipap.corp\Services\DSI\SALOME\BLOC NUMADRESSE\BLOC NUM ADRESSE AVEC CODE BARRE VALIDE POUR C4 ET C6.PDF","24"),
    ("5","cahier des charges PND","\\chsfi005.multipap.corp\Services\DSI\SALOME\PND\Cahier des charges PND.pdf","30"),
    ("6","preconisation PND","\\chsfi005.multipap.corp\Services\DSI\SALOME\PND\Préconisation encodage des fichiers PND.pdf","30"),
    ("7","code OMR","\\chsfi005.multipap.corp\Services\DSI\SALOME\LETTRE CHEQUE\CODE OMR.PDF","34"),
    ("8","code BARRES","\\chsfi005.multipap.corp\Services\DSI\SALOME\CODE BARRE SUR ANNEXE\CODE BARRE.PDF","35"),
    ("9","infos cheque bancaire","\\chsfi005.multipap.corp\Services\DSI\SALOME\LETTRE CHEQUE\informations générique Chèque bancaire.pdf","58"),
    ("10","OPTOSEAL ou Hologramme","\\chsfi005.multipap.corp\Services\DSI\SALOME\LETTRE CHEQUE\OPTOSEAL OU HOLOGRAMME.pdf","59"),
    ("11","cle recomposition","\\chsfi005.multipap.corp\Services\DSI\SALOME\NORME NFK\Norme NFK 11-111 CLE DE RECOMPOSITION.pdf","77"),
    ("12","code CMC7","\\chsfi005.multipap.corp\Services\DSI\SALOME\LETTRE CHEQUE\Code CMC7.pdf","78"),
    ("13","reporting web","\\chsfi005.multipap.corp\Services\DSI\SALOME\REPORTING\PortailWeb_MEP_reporting-MiseEnDemeure.V3.pdf","98"),
    ("14","synthese production client","\\chsfi005.multipap.corp\Services\DSI\SALOME\Mod_DSI_9_Synthèse_Production_CLIENT-PROJET.pdf","113"),
    ("15","guide sepa","\\chsfi005.multipap.corp\Services\DSI\guide_utilisation_tipsepa_v28.pdf","130"),
    ("16","specifications SEPA","\\chsfi005.multipap.corp\Services\DSI\SEPA - SPECIFICATION FONCTIONNELLE ET TECHNIQUES.pdf","130");











