-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema apirecette
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema apirecette
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `apirecette` DEFAULT CHARACTER SET utf8 ;
USE `apirecette` ;

/*grant all privileges on apirecette.* to 'apirecette_user'@'localhost' identified by 'secret';*/
-- -----------------------------------------------------
-- Table `apirecette`.`client`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `apirecette`.`client` (
  `idClient` INT NOT NULL AUTO_INCREMENT,
  `clientName` VARCHAR(45) NULL,
  `clientCode` VARCHAR(45) NULL,
  PRIMARY KEY (`idClient`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `apirecette`.`project`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `apirecette`.`project` (
  `idProject` INT NOT NULL AUTO_INCREMENT,
  `projectName` VARCHAR(45) NOT NULL,
  `fkClient` INT NOT NULL,
  `projectModel` VARCHAR(45) NOT NULL,
  `projectStartDate` DATE NULL,
  `projectEndDate` DATE NULL,
  `projectState` ENUM('actif', 'fini') NULL,
  `projectCreatorName` VARCHAR(45) NULL,
  PRIMARY KEY (`idProject`),
  INDEX `FK_client_idx` (`fkClient` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `apirecette`.`type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `apirecette`.`type` (
  `idtype` INT NOT NULL AUTO_INCREMENT,
  `name_type` VARCHAR(45) NOT NULL,
  `description_type` TEXT NULL,
  PRIMARY KEY (`idtype`))
ENGINE = InnoDB;





-- -----------------------------------------------------
-- Table `apirecette`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `apirecette`.`user` (
  `idUser` INT NOT NULL AUTO_INCREMENT,
  `userName` VARCHAR(45) NOT NULL,
  `userLastname` VARCHAR(45) NULL,
  `userTrigramme` VARCHAR(3) NOT NULL,
  `userLogin` VARCHAR(45) NOT NULL,
  `userPassword` VARCHAR(255) NOT NULL,
  `userCreationDate` DATETIME NULL,
  `userType` ENUM('Dev', 'TIAC','Admin','Compte') NULL,
  PRIMARY KEY (`idUser`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `apirecette`.`assignProjectUser`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `apirecette`.`assignProjectUser` (
  `idProjectUser` INT NOT NULL AUTO_INCREMENT,
  `fkProject` INT NOT NULL,
  `fkUser` INT NOT NULL,
  PRIMARY KEY (`idProjectUser`),
  INDEX `fk_project_idx` (`fkProject` ASC),
  INDEX `fk_user_idx` (`fkUser` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `apirecette`.`Category`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `apirecette`.`category` (
  `idCategory` INT NOT NULL AUTO_INCREMENT,
  `categoryName` VARCHAR(45) NULL,
  PRIMARY KEY (`idCategory`))
ENGINE = InnoDB;




-- -----------------------------------------------------
-- Table `apirecette`.`taskProject`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `apirecette`.`taskProject` (
  `idTaskProject` INT NOT NULL AUTO_INCREMENT,
  `taskProjectName` VARCHAR(45) NOT NULL,
  `taskProjectDescirption` TEXT NULL,
  `taskProjectStatut` ENUM('ok', 'ko', 'na','sans') NULL,
  `taskProjectCommentary` TEXT NULL,
  `taskProjectCategoryName` VARCHAR(45) NULL,
  `taskCreationDate` DATETIME NULL,
  `taskEndDate` DATETIME NULL,
  `fkproject` INT NULL,
  `taskUserType` ENUM('Dev', 'TIAC','Compte') NULL,
  PRIMARY KEY (`idTaskProject`),
INDEX `fk_project_idx` (`fkproject` ASC))
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `apirecette`.`attachedFile`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `apirecette`.`attachedFile` (
  `idAttachedFile` INT NOT NULL AUTO_INCREMENT,
  `attachedFileName` VARCHAR(255) NULL,
  `attachedFilePath` VARCHAR(255) NULL,
  `attachedFileIdTak` INT NULL ,
  PRIMARY KEY (`idAttachedFile`),
   INDEX `fk_taskProject_idx` (`attachedFileIdTak` ASC))
ENGINE = InnoDB;





-- -----------------------------------------------------
-- Table `apirecette`.`Model`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `apirecette`.`model` (
  `idModel` INT NOT NULL AUTO_INCREMENT,
  `modelName` VARCHAR(45) NULL,
 
  PRIMARY KEY (`idModel`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `apirecette`.`linkModelCategory`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `apirecette`.`linkModelCategory` (
  `idLinkModelCategorie` INT NOT NULL AUTO_INCREMENT,
  `fkModel` INT NOT NULL,
  `fkCategory` INT NOT NULL,
  
  PRIMARY KEY (`idLinkModelCategorie`),
  INDEX `fk_category_idx` (`fkCategory` ASC),
  INDEX `fk_model_idx` (`fkModel` ASC))
ENGINE = InnoDB;








-- -----------------------------------------------------
-- Table `apirecette`.`logTaskHistory`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `apirecette`.`logTaskHistory` (
  `idLogTaskHistory` INT NOT NULL AUTO_INCREMENT,
  `fkTask` INT NULL,
  `dateLogTask` DATETIME NULL,
  `userTrigramme` VARCHAR(4) NULL,
  `typeLog` ENUM('ok','ko','na','commentaire','fichier') NULL,
  `commentary` TEXT NULL,
  PRIMARY KEY (`idLogTaskHistory`),
  INDEX `fk_taches_idx` (`fkTask` ASC))
ENGINE = InnoDB;





-- -----------------------------------------------------
-- Table `apirecette`.`typeLogProject`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `apirecette`.`typeLogProject` (
  `idTypeLogProject` INT NOT NULL AUTO_INCREMENT,
  `descriptionTypeLogProject` VARCHAR(45) NULL,
  PRIMARY KEY (`idTypeLogProject`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `apirecette`.`logProjectHistory`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `apirecette`.`logProjectHistory` (
  `idLogProjectHistory` INT NOT NULL AUTO_INCREMENT,
  `typeLog` INT NULL,
  `fkProject` INT NULL,
  `fkUser` INT NULL,
  `dateLogProject` DATETIME NULL,
  PRIMARY KEY (`idLogProjectHistory`),
  INDEX `fk_project_idx` (`fkProject` ASC),
  INDEX `fk_user_idx` (`fkUser` ASC),
  INDEX `fk_type_log_idx` (`typeLog` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `apirecette`.`Task`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `apirecette`.`task` (
  `idTask` INT NOT NULL AUTO_INCREMENT,
  `taskName` VARCHAR(255) NULL,
  `fk_category` INT NULL,
  `taskDescription` TEXT NULL,
  `categoryTask`VARCHAR(45) NULL,
  `userType` VARCHAR(4) NULL,
  PRIMARY KEY (`idTask`),
  INDEX `fk_category_idx` (`fk_category` ASC))
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `apirecette`.`modelattachedFile`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `apirecette`.`modelattachedFile` (
  `idModelAttachedFile` INT NOT NULL AUTO_INCREMENT,
  `modelAttachedFileName` VARCHAR(255) NULL,
  `modelAttachedFilePath` VARCHAR(255) NULL,
  `modelAttachedFileIdTak` INT NULL ,
  PRIMARY KEY (`idModelAttachedFile`),
   INDEX `fk_task_idx` (`modelAttachedFileIdTak` ASC))
ENGINE = InnoDB;





SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
